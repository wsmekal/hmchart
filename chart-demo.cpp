/* ------------------------------------------------------------------------ */
/* chart-demo.cpp - Test bzw. Demonstration von HMChart                     */
/*                                                                          */
/*                  Copyright Tilman Kuepper 2020.                          */
/*    Distributed under the Boost Software License, Version 1.0.            */
/*        (See accompanying file LICENSE_1_0.txt or copy at                 */
/*               http://www.boost.org/LICENSE_1_0.txt)                      */
/*                                                                          */
/* Letzte Bearbeitung: 27. Juli 2020                                        */
/* ------------------------------------------------------------------------ */

#define _USE_MATH_DEFINES
#include "chart.h"
#include <stdlib.h>
#include <math.h>
#include <list>
#include <vector>

void liniengrafik(void)
{
    /* Haus */
    chart_moveto(2, 0);
    chart_lineto(2, 2, CHART_BLUE);
    chart_lineto(1, 3, CHART_BLUE);
    chart_lineto(0, 2, CHART_BLUE);
    chart_lineto(2, 2, CHART_BLUE);
    chart_lineto(0, 0, CHART_BLUE);
    chart_lineto(0, 2, CHART_BLUE);
    chart_lineto(2, 0, CHART_BLUE); chart_lineto(0, 0, CHART_BLUE);

    /* Wiese */
    chart_moveto(-1.0, -0.05); chart_lineto( 3.0, -0.05, CHART_GREEN);
    chart_moveto(-1.5, -0.10); chart_lineto( 3.5, -0.10, CHART_GREEN);

    /* Grafik anzeigen */
    chart_axis_xlimit(-2.0, 4.0);
    chart_axis_ylimit(-0.5, 3.5);
    chart_show(CHART_NOBACKGROUND | CHART_NOGRID | CHART_XLIMIT
        | CHART_YLIMIT | CHART_XZOOM_OFF | CHART_YZOOM_OFF,
        "Liniengrafik, manuelle Achsenskalierung, Zoomfunktion deaktiviert");
}

void punkte_und_linien(void)
{
    /* Startposition fuer Sinuskurve */
    double x, y;
    x = -5; y = sin(x); chart_moveto(x, y);

    /* Sinuskurve zeichnen */
    for(x = -5; x < 5; x += 0.1)
    {
        y = sin(x);
        chart_lineto(x, y, CHART_BLACK);
    }

    /* Startposition fuer Kosinuskurve */
    x = -5; y = cos(x); chart_moveto(x, y);

    /* Kosinuskurve zeichnen */
    for(x = -5; x < 5; x += 0.1)
    {
        y = cos(x);
        chart_lineto(x, y, CHART_GREEN);
    }

    /* Verlauf von sin(x) * cos(x) mit Punkten zeichnen */
    for(x = -5; x < 5; x += 0.1)
    {
        y = sin(x) * cos(x);
        chart_point(x, y, CHART_RED);
    }

    /* Chart auf dem Bildschirm darstellen */
    chart_show(CHART_DEFAULT, "Punkte und Linien");
}

void achsenbeschriftung(void)
{
    double t;
    double omega = 2 * 3.1416 * 50;
    double effektivwert = 230;

    /* Sinuskurve zeichnen */
    for(t = -25; t < 40; t += 0.1)
    {
        double u = sqrt(2.0) * effektivwert * sin(omega * t / 1000.0);

        /* 5. Mai 2016: Auch chart_size() testen! */
        if(chart_size() == 0) chart_moveto(t, u);
        chart_lineto(t, u, CHART_RED);
    }

    /* Format fuer x- und y-Achse uebergeben (wie bei printf) */
    chart_axis_fmt("%.1f ms", "%.1f Volt");

    /* Chart darstellen */
    chart_show(CHART_DEFAULT, "Einstellung der Achsenbeschriftung");
}

void axis_limit(void)
{
    /* Funktion y = x * sin(1.0 / x) grafisch darstellen */
    double x = -1.0, y = x * sin(1.0 / x);
    chart_moveto(x, y);
    for(x = -1.0; x < 1.0; x += 0.00002)
    {
        y = x * sin(1.0 / x);
        chart_lineto(x, y, CHART_BLUE);
    }

    /* Der x-Bereich hat eigentlich eine Ausdehnung von -1.0 ... 1.0 */
    chart_axis_xlimit(-0.05, 0.15);

    /* Der y-Bereich hat eigentlich eine Ausdehnung von -0.842 ... 0.842 */
    chart_axis_ylimit(-0.075, 0.075);

    /* Manuelle Achsenskalierung aktivieren, Y-Zoom deaktivieren */
    chart_show(CHART_XLIMIT | CHART_YLIMIT | CHART_YZOOM_OFF,
        "Manuelle Achsenskalierung aktiviert, Y-Zoom deaktiviert");
}

void animation(void)
{
    /* Animiertes Polynom */
    for(double param = -2; param < 2; param += 0.05)
    {
        std::vector<double> y;
        for(double x = -75; x < 25; x++)
            y.push_back(param * x * x * x + 50 * param * x * x + x);

        /* C++-Container koennen direkt uebergeben werden; fehlende
         * x-Werte werden automatisch zu 0, 1, 2, 3 usw. gesetzt. */
        chart_line_series (y, CHART_GRAY);
        chart_point_series(y,  CHART_RED);
        chart_end_of_frame();
    }

    /* Jedes Teilbild wird 100 msec lang angezeigt */
    chart_frame_timer(100);
    chart_show(CHART_DEFAULT | CHART_NOBACKGROUND, "Animiertes Polynom");
}

void background(void)
{
    /* Bunte Punkte auf schwarzem Hintergrund */
    chart_background(0, 0, 0);

    for(int i = 0; i < 5000; ++i)
    {
        double x = 2.0 * rand() / RAND_MAX - 1.0; 
        double y = 2.0 * rand() / RAND_MAX - 1.0; 
        int col = rand() % 5;
        if(x * x + y * y < 1) chart_point(x, y, col);
    }

    chart_show(CHART_NOGRID | CHART_NOCLEAR, "Schwarzer Hintergrund (a)");

    /* Nun kommen noch einige Linien dazu,
     * wegen CHART_NOCLEAR bleiben die Punkte bestehen. */
    for(int i = 0; i < 500; ++i)
    {
        double x = 2.0 * rand() / RAND_MAX - 1.0;
        double y = 2.0 * rand() / RAND_MAX - 1.0;
        int col = rand() % 5;
        if(x * x + y * y < 1) chart_lineto(x, y, col);
    }

    chart_show(CHART_NOGRID, "Schwarzer Hintergrund (b)");
}

int main(void)
{
    punkte_und_linien();
    liniengrafik();
    achsenbeschriftung();
    axis_limit();
    background();
    animation();
    return 0;
}
