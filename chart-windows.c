/* ######################################################################## */
/* ###  USE THIS FILE "CHART-WINDOWS.C" FOR MICROSOFT WINDOWS PROJECTS! ### */
/* ###  DIESE DATEI "CHART-WINDOWS.C" FUER MICROSOFT WINDOWS VERWENDEN! ### */
/* ######################################################################## */

/* ######################################################################## */
/* ###  Bei der Erstellung von Microsoft-Windows-Projekten muessen die  ### */
/* ###  Bibliotheken user32.lib, gdi32.lib und comdlg32.lib zum Projekt ### */
/* ###  hinzugefuegt werden. Bei Verwendung von Microsoft Visual C++    ### */
/* ###  geschieht dies automatisch.                                     ### */
/* ######################################################################## */

/* ------------------------------------------------------------------------ */
/* chart.c - Ausgabe von 2D-Charts unter Microsoft Windows                  */
/*                                                                          */
/*                  Copyright Tilman Kuepper 2020.                          */
/*    Distributed under the Boost Software License, Version 1.0.            */
/*        (See accompanying file LICENSE_1_0.txt or copy at                 */
/*               http://www.boost.org/LICENSE_1_0.txt)                      */
/*                                                                          */
/* Letzte Bearbeitung: 27. Juli 2020                                        */
/* ------------------------------------------------------------------------ */

#undef  _CRT_SECURE_NO_WARNINGS
#define _CRT_SECURE_NO_WARNINGS  /* Warnungen bei MSVC++ ausschalten */

#ifdef _MSC_VER
    #pragma comment(lib, "user32.lib")
    #pragma comment(lib, "gdi32.lib")
    #pragma comment(lib, "comdlg32.lib")
#endif

/* Darauf achten,  dass bei UNICODE-Projekten sowohl */
/* UNICODE als auch _UNICODE zugleich definiert sind */
#ifdef UNICODE
#undef _UNICODE
#define _UNICODE
#endif

#ifdef _UNICODE
#undef UNICODE
#define UNICODE
#endif

#include <math.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "chart.h"

/* Spezielle Headerdateien fuer Windows-Version des Charts... */
#include "windows.h"
#include "tchar.h"

/* Max. Anzahl der Objekte im Chart: Linien, Punkte usw... */
#ifndef CHART_MAX_OBJECTS
#define CHART_MAX_OBJECTS 100000
#endif

#undef MIN_WIDTH
#undef MIN_HEIGHT
#undef GRID_WIDTH_THR
#undef GRID_HEIGTH_THR
#undef ZOOM_STEP
#undef ZOOM_MAX
#undef SERIES_WIDTH
#undef MIN_PT_SIZE
#undef TEXT_BUFSIZE
#undef CHECK_OVERLAP
#undef AXIS_FMT_LEN
#undef AXIS_DEFLT_FMT
#undef AXIS_DEFLT_LIM
#undef PRINTER_DOCNAME
#undef WINDOW_CLASSNAME
#undef CHART_COLOR
#undef GRID_COLOR
#undef BLACK_COLOR
#undef RED_COLOR
#undef GREEN_COLOR
#undef BLUE_COLOR
#undef GRAY_COLOR
#undef DEFAULT_TITLE
#undef MAX
#undef MIN

#define MIN_WIDTH        400          /* Min. Fensterbreite                 */
#define MIN_HEIGHT       200          /* Min. Fensterhoehe                  */
#define GRID_WIDTH_THR   800          /* Ab dieser Fenstergroesse gibt es   */
#define GRID_HEIGTH_THR  400          /* mehr Gitterlinien (Defaultwert)    */
#define ZOOM_STEP        1.25         /* Zoomfaktor-Aenderung bei Mausklick */
#define ZOOM_MAX         64.0         /* Max. Zoomfaktor                    */
#define SERIES_WIDTH     2            /* Linienbreite fuer chart_lineto()   */
#define MIN_PT_SIZE      2            /* Markierungen bei chart_point()     */
#define TEXT_BUFSIZE     1024         /* Textbuffer fuer Achsenbeschriftung */
#define CHECK_OVERLAP    50           /* "Kollision" von x/y-Labels pruefen */
#define AXIS_FMT_LEN     128          /* Formatstr. zur Achsenbeschriftung  */
#define AXIS_DEFLT_FMT   "%g"         /* Standardformat fuer x-/y-Achse     */
#define AXIS_DEFLT_LIM   1.0          /* Fuer chart_axis_xlimit()/_ylimit() */

#define PRINTER_DOCNAME  "Chart"                  /* Siehe print()...       */
#define WINDOW_CLASSNAME "ChartWindowClass"       /* Siehe chart_show()...  */
#define CHART_COLOR      RGB(235, 235, 235)       /* Hintergrundfarbe       */
#define GRID_COLOR       RGB(150, 150, 150)       /* Farbe der Gitterlinien */
#define BLACK_COLOR      RGB(  0,   0,   0)       /* Schwarze Linienfarbe   */
#define RED_COLOR        RGB(255,   0,   0)       /* Rote Linienfarbe       */
#define GREEN_COLOR      RGB(  0, 255,   0)       /* Gruene Linienfarbe     */
#define BLUE_COLOR       RGB(  0,   0, 255)       /* Blaue Linienfarbe      */
#define GRAY_COLOR       RGB(222, 222, 222)       /* Graue Linienfarbe      */
#define DEFAULT_TITLE    "Chart"                  /* Siehe chart_show()...  */
#define MAX(a, b)        ((a)>(b)?(a):(b))        /* Maximum bestimmen      */
#define MIN(a, b)        ((a)<(b)?(a):(b))        /* Minimum bestimmen      */


/* ------------------------------------------------------------------------ */
/* Sollen Hintergrund und/oder Koordinatensystem dargestellt werden?        */
/* ------------------------------------------------------------------------ */
#undef HAVE_BACKGROUND
#undef HAVE_GRID

#define HAVE_BACKGROUND(opt) (((opt) & CHART_NOBACKGROUND) == 0)
#define HAVE_GRID(opt)       (((opt) & CHART_NOGRID      ) == 0)

/* ------------------------------------------------------------------------ */
/* Struktur zum Speichern der min./max. Weltkoordinaten der Objekte und     */
/* der Chart-Abmessungen (in Pixeln); wird von setup_viewport() gefuellt.   */
/* ------------------------------------------------------------------------ */
typedef struct tagCHART_VIEWPORT
{
    double world_x1, world_y1, world_x2, world_y2;
    int    chart_x1, chart_y1, chart_x2, chart_y2;
    double world_width, world_height;
    int    chart_width, chart_height;
} CHART_VIEWPORT;

/* ------------------------------------------------------------------------ */
/* Im Chart befinden sich Punkte und Linien, die ggf. auf mehrere           */
/* Einzelbilder verteilt sind. Die Daten dieser "Chart-Objekte" werden      */
/* in der folgenden Struktur erfasst.                                       */
/* ------------------------------------------------------------------------ */
enum { POINT_OBJECT, LINE_OBJECT, END_OF_FRAME };
typedef struct tagOBJECT
{
    int type;  /* POINT_OBJECT, LINE_OBJECT oder END_OF_FRAME */
    double x1, y1, x2, y2;
    int color;
} OBJECT;

/* Hier ist der komplette Chart-Inhalt gespeichert. */
static OBJECT chart_objects[CHART_MAX_OBJECTS];

/* ------------------------------------------------------------------------ */
/* Struktur mit aktueller Objekt-Anzahl, dem aktuellem Zoom-Zustand und     */
/* den gewuenschten Darstellungsoptionen (mit/ohne Koordinatensystem).      */
/* ------------------------------------------------------------------------ */
typedef struct tagCHART_DATA
{
    size_t num_objects, current_frame_start;
    double current_x, current_y;
    double zoom_factor, zoom_x, zoom_y;
    char axis_xfmt[AXIS_FMT_LEN], axis_yfmt[AXIS_FMT_LEN];
    int options, frame_timer_msec;
    double axis_xmin, axis_xmax, axis_ymin, axis_ymax;
    int grid_width_threshold, grid_heigth_threshold;
    COLORREF background_color;
} CHART_DATA;

/* Hier ist der aktuelle Chart-Zustand gespeichert. */
static CHART_DATA chart_state =
{
    0, 0,                               /* num_objects, current_frame_start */
    0.0, 0.0,                           /* current_x, current_y             */
    0.0, 0.0, 0.0,                      /* zoom_factor, zoom_x, zoom_y      */
    AXIS_DEFLT_FMT, AXIS_DEFLT_FMT,     /* axis_xfmt, axis_yfmt             */
    CHART_DEFAULT, 1000,                /* options, frame_timer_msec        */
    -AXIS_DEFLT_LIM, +AXIS_DEFLT_LIM,   /* axis_xmin, axis_xmax             */
    -AXIS_DEFLT_LIM, +AXIS_DEFLT_LIM,   /* axis_ymin, axis_ymax             */
    GRID_WIDTH_THR, GRID_HEIGTH_THR,    /* grid_width/heigth_threshold      */
    CHART_COLOR                         /* background_color                 */
};

/* ------------------------------------------------------------------------ */
/* HINSTANCE dieses Programms ermitteln. Siehe www.codeguru.com: Detecting  */
/* a HMODULE/HINSTANCE handle within the module you're running in.          */
/* ------------------------------------------------------------------------ */
static HINSTANCE get_instance(void) 
{ 
    MEMORY_BASIC_INFORMATION mbi;
    static int dummy;
    VirtualQuery(&dummy, &mbi, sizeof(mbi));
    return (HINSTANCE)(mbi.AllocationBase);
} 

/* ------------------------------------------------------------------------ */
/* Die min./max. Weltkoordinaten der Punkte und Linien im Chart sowie die   */
/* Chart-Abmessungen in Pixeln werden ermittelt und in der uebergebenen     */
/* CHART_VIEWPORT-Struktor abgelegt. Diese Funktion wird zu Beginn der      */
/* Ausgabe auf dem Bildschirm oder Drucker aufgerufen.                      */
/* ------------------------------------------------------------------------ */
static void setup_viewport(const RECT *rc, CHART_VIEWPORT *v)
{
    size_t i;

    assert(rc);
    assert(v);

    /* Abmessungen des Charts in Pixeln */
    v->chart_x1 = rc->left; v->chart_x2 = rc->right;
    v->chart_y1 = rc->top;  v->chart_y2 = rc->bottom;

    /* Suche nach den min./max. Weltkoordinaten */
    v->world_x1 = chart_state.num_objects ? chart_objects[0].x1 : 0;
    v->world_y1 = chart_state.num_objects ? chart_objects[0].y1 : 0;
    v->world_x2 = v->world_x1; v->world_y2 = v->world_y1;

    for(i = 0; i < chart_state.num_objects; ++i)
    {
        const OBJECT *obj = &chart_objects[i];
        if(obj->type == END_OF_FRAME) continue;

        v->world_x1 = MIN(obj->x1, v->world_x1);
        v->world_x2 = MAX(obj->x1, v->world_x2);
        v->world_y1 = MIN(obj->y1, v->world_y1);
        v->world_y2 = MAX(obj->y1, v->world_y2);

        if(obj->type != LINE_OBJECT) continue;
        v->world_x1 = MIN(obj->x2, v->world_x1);
        v->world_x2 = MAX(obj->x2, v->world_x2);
        v->world_y1 = MIN(obj->y2, v->world_y1);
        v->world_y2 = MAX(obj->y2, v->world_y2);
    }

    /* Wurden die Achsen evtl. manuell eingestellt? */
    if(chart_state.options & CHART_XLIMIT)
        v->world_x1 = chart_state.axis_xmin, v->world_x2 = chart_state.axis_xmax;
    if(chart_state.options & CHART_YLIMIT)
        v->world_y1 = chart_state.axis_ymin, v->world_y2 = chart_state.axis_ymax;

    /* Aktuellen Zoom-Faktor beruecksichtigen */
    if(chart_state.zoom_factor > 1 && !(chart_state.options & CHART_XZOOM_OFF))
    {
        double world_width;
        world_width  = (v->world_x2 - v->world_x1) / chart_state.zoom_factor;
        v->world_x1 = chart_state.zoom_x - world_width  / 2.0;
        v->world_x2 = chart_state.zoom_x + world_width  / 2.0;
    }
    if(chart_state.zoom_factor > 1 && !(chart_state.options & CHART_YZOOM_OFF))
    {
        double world_height;
        world_height = (v->world_y2 - v->world_y1) / chart_state.zoom_factor;
        v->world_y1 = chart_state.zoom_y - world_height / 2.0;
        v->world_y2 = chart_state.zoom_y + world_height / 2.0;
    }

    /* Sonderbehandlung fuer Achsenbeschriftung bei leeren Charts */
    if(v->world_x1 == v->world_x2) (v->world_x1 -= 1), (v->world_x2 += 1);
    if(v->world_y1 == v->world_y2) (v->world_y1 -= 1), (v->world_y2 += 1);

    v->world_width  = v->world_x2 - v->world_x1;
    v->world_height = v->world_y2 - v->world_y1;
    v->chart_width  = v->chart_x2 - v->chart_x1;
    v->chart_height = v->chart_y2 - v->chart_y1;
}

/* ------------------------------------------------------------------------ */
/* X-Weltkoordinate in X-Position (in Pixeln) umrechnen.                    */
/* ------------------------------------------------------------------------ */
static int x_to_screen(const CHART_VIEWPORT *v, double world_x)
{
    double factor = (world_x - v->world_x1) / v->world_width;
    return v->chart_x1 + (int)(factor * v->chart_width);
}

/* ------------------------------------------------------------------------ */
/* Y-Weltkoordinate in Y-Position (in Pixeln) umrechnen.                    */
/* ------------------------------------------------------------------------ */
static int y_to_screen(const CHART_VIEWPORT *v, double world_y)
{
    double factor = (world_y - v->world_y1) / v->world_height;
    return v->chart_y2 - (int)(factor * v->chart_height);
}

/* ------------------------------------------------------------------------ */
/* X-Position (in Pixeln) in X-Weltkoordinate umrechnen.                    */
/* ------------------------------------------------------------------------ */
static double screen_to_x(const CHART_VIEWPORT *v, int screen_x)
{
    double factor = 1.0 * (screen_x - v->chart_x1) / v->chart_width;
    return v->world_x1 + (factor * v->world_width);
}

/* ------------------------------------------------------------------------ */
/* Y-Position (in Pixeln) in Y-Weltkoordinate umrechnen.                    */
/* ------------------------------------------------------------------------ */
static double screen_to_y(const CHART_VIEWPORT *v, int screen_y)
{
    double factor = 1.0 * (screen_y - v->chart_y1) / v->chart_height;
    return v->world_y2 - (factor * v->world_height);
}

/* ------------------------------------------------------------------------ */
/* Der Menuepunkt "Show/Hide Grid" wurde gewaehlt. Es werden abwechselnd    */
/* die Hintergrundfarbe und die Gitterlinien ein- bzw. ausgeschaltet.       */
/* ------------------------------------------------------------------------ */
static void grid_on_off(HWND hwnd)
{
    assert(hwnd);

    if(HAVE_GRID(chart_state.options))
        chart_state.options += CHART_NOGRID;
    else if(HAVE_BACKGROUND(chart_state.options))
        chart_state.options -= CHART_NOGRID,
        chart_state.options += CHART_NOBACKGROUND;
    else
        chart_state.options -= CHART_NOGRID,
        chart_state.options -= CHART_NOBACKGROUND;

    InvalidateRect(hwnd, NULL, TRUE);
}

/* ------------------------------------------------------------------------ */
/* Linke Maustaste gedrueckt: Ansicht wird um eine Stufe vergroessert.      */
/* ------------------------------------------------------------------------ */
static void zoom_in(HWND hwnd, LPARAM lp)
{
    CHART_VIEWPORT v;
    RECT rc;

    /* Funktion abbrechen, wenn Zoom-Funktion komplett deaktiviert ist */
    const int options = chart_state.options;
    if((options & CHART_XZOOM_OFF) && (options & CHART_YZOOM_OFF)) return;

    assert(hwnd);
    GetClientRect(hwnd, &rc);
    setup_viewport(&rc, &v);
    chart_state.zoom_x = screen_to_x(&v, LOWORD(lp) /* x_pos */ );
    chart_state.zoom_y = screen_to_y(&v, HIWORD(lp) /* y_pos */ );

    chart_state.zoom_factor *= ZOOM_STEP;
    if(chart_state.zoom_factor > ZOOM_MAX) MessageBeep(MB_ICONINFORMATION);
    chart_state.zoom_factor = MAX(chart_state.zoom_factor, ZOOM_STEP);
    chart_state.zoom_factor = MIN(chart_state.zoom_factor, ZOOM_MAX );

    InvalidateRect(hwnd, NULL, TRUE);
}

/* ------------------------------------------------------------------------ */
/* Urspruengliche Ansicht bzw. Vergroesserungsstufe wiederherstellen.       */
/* ------------------------------------------------------------------------ */
static void zoom_out(HWND hwnd)
{
    assert(hwnd);
    chart_state.zoom_factor = 0;
    InvalidateRect(hwnd, NULL, TRUE);
}

/* ------------------------------------------------------------------------ */
/* Farbigen Hintergrund ausgeben                                            */
/* ------------------------------------------------------------------------ */
static void paint_background(HDC hdc, const CHART_VIEWPORT *v)
{
    RECT rc;
    HBRUSH chart_brush;

    assert(hdc);
    assert(v);

    rc.left = v->chart_x1; rc.right  = v->chart_x2;
    rc.top  = v->chart_y1; rc.bottom = v->chart_y2;
    chart_brush = CreateSolidBrush(chart_state.background_color);
    FillRect(hdc, &rc, chart_brush);
    DeleteObject(chart_brush);
}

/* ------------------------------------------------------------------------ */
/* Koordinatensystem ausgeben.                                              */
/* ------------------------------------------------------------------------ */
static void paint_grid(HDC hdc, const CHART_VIEWPORT *v)
{
    char text[TEXT_BUFSIZE];
    double x, y, exp10, xstart, xwidth, xstep, ystart, ywidth, ystep;
    int font_height = -MulDiv(11, GetDeviceCaps(hdc, LOGPIXELSY), 72);
    int x_scr = 0, y_scr = 0, last_y_scr = 0;
    HGDIOBJ pen, old_pen, font, old_font;
    assert(hdc); assert(v);

    /* 2017-06-04, Schrift zur Achsenbeschriftung wird nun explizit
     * neu erstellt und in den hdc geladen, da sonst bei manchen
     * Druckertreibern keine Beschriftung ausgegeben wird. */
    font = (HGDIOBJ)CreateFont(font_height, 0, 0, 0, FW_NORMAL, FALSE,
        FALSE, FALSE, ANSI_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS,
        DEFAULT_QUALITY, DEFAULT_PITCH | FF_DONTCARE, 0);
    assert(font);
    old_font = SelectObject(hdc, font);
    SetTextColor(hdc, BLACK_COLOR);

    pen = (HGDIOBJ)CreatePen(PS_DOT, 0, GRID_COLOR);
    assert(pen);
    old_pen = SelectObject(hdc, pen);
    SetBkMode(hdc, TRANSPARENT);

    /* Welcher x-Bereich muss dargestellt werden? */
    xwidth = v->world_width, exp10 = 0;
    while(xwidth <  1) xwidth *= 10, --exp10;
    while(xwidth > 10) xwidth /= 10, ++exp10;

    /* Bei kleinen Fenstern -> weniger Gitterlinien zeichnen. */
    if(v->chart_width < chart_state.grid_width_threshold) xwidth *= 2.0;

    /* Schrittweite der x-Gitterlinien ermitteln */
    if     (xwidth > 8.0) xstep = 2.0 * pow(10, exp10);
    else if(xwidth > 4.0) xstep = 1.0 * pow(10, exp10);
    else if(xwidth > 2.0) xstep = 0.5 * pow(10, exp10);
    else                  xstep = 0.2 * pow(10, exp10);

    /* Position/Koordinate der ersten x-Gitterlinie ermitteln. */
    xstart = floor(v->world_x1 / pow(10, exp10 + 1)) * pow(10, exp10 + 1);
    if(v->world_x1 * v->world_x2 <= 0) xstart = 0;
    while(xstart  > v->world_x1) xstart -= xstep;
    while(xstart <= v->world_x1) xstart += xstep;

    /* Welcher y-Bereich muss dargestellt werden? */
    ywidth = v->world_height, exp10 = 0;
    while(ywidth <  1) ywidth *= 10, --exp10;
    while(ywidth > 10) ywidth /= 10, ++exp10;

    /* Bei kleinen Fenstern -> weniger Gitterlinien zeichnen. */
    if(v->chart_height < chart_state.grid_heigth_threshold) ywidth *= 2.0;

    /* Schrittweite der y-Gitterlinien ermitteln */
    if     (ywidth > 6.0) ystep = 2.0 * pow(10, exp10);
    else if(ywidth > 3.0) ystep = 1.0 * pow(10, exp10);
    else if(ywidth > 1.5) ystep = 0.5 * pow(10, exp10);
    else                  ystep = 0.2 * pow(10, exp10);

    /* Position/Koordinate der ersten y-Gitterlinie ermitteln. */
    ystart = floor(v->world_y1 / pow(10, exp10 + 1)) * pow(10, exp10 + 1);
    if(v->world_y1 * v->world_y2 <= 0) ystart = 0;
    while(ystart >  v->world_y1) ystart -= ystep;
    while(ystart <= v->world_y1) ystart += ystep;

    /* y-Beschriftungen ausgeben */
    for(y = ystart; y < v->world_y2; y += ystep)
    {
        /* Trick, damit Null-Linie nicht mit "1.3e-16" beschriftet wird. */
        if(fabs(y) < v->world_height / 1000.0) y = 0;
        sprintf(text, chart_state.axis_yfmt, y);
        x_scr = x_to_screen(v, v->world_x1), y_scr = y_to_screen(v, y);
        TextOutA(hdc, 3 + x_scr, 1 + y_scr, text, (int)strlen(text));
    }
    last_y_scr = y_scr;

    /* x-Beschriftungen ausgeben */
    for(x = xstart; x < v->world_x2; x += xstep)
    {
        /* x-Werte nicht ausgeben, wenn die Gefahr besteht, dass sich
         * x- und y-Werte in der Fensterecke ueberschneiden. */
        x_scr = x_to_screen(v, x), y_scr = y_to_screen(v, v->world_y2);
        if(x_scr < CHECK_OVERLAP && last_y_scr < CHECK_OVERLAP) continue;

        /* Trick, damit Null-Linie nicht mit "1.3e-16" beschriftet wird. */
        if(fabs(x) < v->world_width  / 1000.0) x = 0;
        sprintf(text, chart_state.axis_xfmt, x);
        TextOutA(hdc, 3 + x_scr, 1 + y_scr, text, (int)strlen(text));
    }

    /* x-Gitterlinien zeichnen */
    for(x = xstart; x < v->world_x2; x += xstep)
    {
        MoveToEx(hdc, x_to_screen(v, x), y_to_screen(v, v->world_y1), 0);
        LineTo  (hdc, x_to_screen(v, x), y_to_screen(v, v->world_y2));
    }

    /* y-Gitterlinien zeichnen */
    for(y = ystart; y < v->world_y2; y += ystep)
    {
        MoveToEx(hdc, x_to_screen(v, v->world_x1), y_to_screen(v, y), 0);
        LineTo  (hdc, x_to_screen(v, v->world_x2), y_to_screen(v, y));
    }

    SelectObject(hdc,  old_pen); DeleteObject( pen);
    SelectObject(hdc, old_font); DeleteObject(font);
}

/* ------------------------------------------------------------------------ */
/* Punkte und Linien ausgeben.                                              */
/* ------------------------------------------------------------------------ */
static void paint_chart(HDC hdc, const CHART_VIEWPORT *v)
{
    size_t i;
    int x, x1, x2, y, y1, y2, p_size, color = CHART_BLACK;

    /* Stifte mit allen moeglichen Farben der Chart-Objekte erzeugen */
    HGDIOBJ black = (HGDIOBJ)CreatePen(PS_SOLID, SERIES_WIDTH, BLACK_COLOR);
    HGDIOBJ red   = (HGDIOBJ)CreatePen(PS_SOLID, SERIES_WIDTH,   RED_COLOR);
    HGDIOBJ green = (HGDIOBJ)CreatePen(PS_SOLID, SERIES_WIDTH, GREEN_COLOR);
    HGDIOBJ blue  = (HGDIOBJ)CreatePen(PS_SOLID, SERIES_WIDTH,  BLUE_COLOR);
    HGDIOBJ gray  = (HGDIOBJ)CreatePen(PS_SOLID, SERIES_WIDTH,  GRAY_COLOR);
    HGDIOBJ bak   = SelectObject(hdc, black);

    assert(hdc ); assert(v    );
    assert(red ); assert(black); assert(blue); assert(green); assert(gray);

    /* Groesse von Punkt-Markierungen im Chart, vergl. chart_point().
       Diese Markierungen werden auf dem Bildschirm mit weniger Pixeln
       ausgegeben als auf dem Drucker, weil auf dem Drucker i. d. R.
       eine hoehere Aufloesung (Pixel pro Zoll) gegeben ist. */
    p_size = GetDeviceCaps(hdc, LOGPIXELSX) / 50;
    if(p_size < MIN_PT_SIZE) p_size = MIN_PT_SIZE;

    /* Die Chart-Objekte werden der Reihe nach durchlaufen und ausgeben.
       Die Ausgabe wird abgebrochen, falls das Ende der Objektliste erreicht
       wurde oder - bei einem animierten Chart - wenn das aktuelle Einzelbild
       vollstaendig ausgegeben wurde. */
    for(i = chart_state.current_frame_start; i < chart_state.num_objects; ++i)
    {
        const OBJECT *obj = &chart_objects[i];
        if(obj->type == END_OF_FRAME) break;  /* Ende des Einzelbildes! */

        if(color != obj->color)
        {
            switch(obj->color)
            {
            case CHART_BLACK: SelectObject(hdc, black); break;
            case CHART_RED:   SelectObject(hdc, red  ); break;
            case CHART_GREEN: SelectObject(hdc, green); break;
            case CHART_BLUE:  SelectObject(hdc, blue ); break;
            case CHART_GRAY:  SelectObject(hdc, gray);  break;
            default: assert(!"invalid color");
            }
            color = obj->color;
        }

        switch(obj->type)
        {
        case LINE_OBJECT:
            x1 = x_to_screen(v, obj->x1); y1 = y_to_screen(v, obj->y1);
            x2 = x_to_screen(v, obj->x2); y2 = y_to_screen(v, obj->y2);
            MoveToEx(hdc, x1, y1, NULL); LineTo(hdc, x2, y2);
            break;
        case POINT_OBJECT:
            x = x_to_screen(v, obj->x1); y = y_to_screen(v, obj->y1);
            Rectangle(hdc, x - p_size, y - p_size, x + p_size, y + p_size);
            break;
        default:
            assert(!"invalid object type");
        }
    }

    SelectObject(hdc, bak);
    DeleteObject(black); DeleteObject(blue); DeleteObject(red);
    DeleteObject(green); DeleteObject(gray); 
}

/* ------------------------------------------------------------------------ */
/* Hier erfolgt die eigentliche Ausgabe des Charts (inkl. Hintergrund) in   */
/* einem bestimmten Koordinatenbereich (rc) des angegebenen Fensters (hdc). */
/* ------------------------------------------------------------------------ */
static void paint_hdc(RECT rc, HDC hdc)
{
    CHART_VIEWPORT v;
    assert(hdc);

    setup_viewport(&rc, &v);
    if(HAVE_BACKGROUND(chart_state.options)) paint_background(hdc, &v);
    if(HAVE_GRID(chart_state.options)) paint_grid(hdc, &v);
    paint_chart(hdc, &v);
}

/* ------------------------------------------------------------------------ */
/* Chart (inkl. Hintergrund) auf Bildschirm ausgeben.                       */
/* ------------------------------------------------------------------------ */
static void paint(HWND hwnd)
{
    PAINTSTRUCT ps; HDC hdc; RECT rc;
    assert(hwnd);

    GetClientRect(hwnd, &rc);
    hdc = BeginPaint(hwnd, &ps);
    paint_hdc(rc, hdc);
    EndPaint(hwnd, &ps);
}

/* ------------------------------------------------------------------------ */
/* Chart (inkl. Hintergrund) auf Drucker ausgeben.                          */
/* ------------------------------------------------------------------------ */
static void print(HWND hwnd)
{
    CHART_VIEWPORT v; RECT rc;
    DOCINFO di; PRINTDLG pd;

    assert(hwnd);
    ZeroMemory(&di, sizeof(di));
    di.cbSize = sizeof(di);
    di.lpszDocName = _T(PRINTER_DOCNAME);

    ZeroMemory(&pd, sizeof(pd));
    pd.lStructSize = sizeof(pd);
    pd.hwndOwner = hwnd;
    pd.Flags = PD_RETURNDC;

    if(PrintDlg(&pd) && pd.hDC)
    {
        rc.left = 0; rc.right  = GetDeviceCaps(pd.hDC, HORZRES);
        rc.top  = 0; rc.bottom = GetDeviceCaps(pd.hDC, VERTRES);
        setup_viewport(&rc, &v);

        StartDoc(pd.hDC, &di); StartPage(pd.hDC);
        if(HAVE_BACKGROUND(chart_state.options)) paint_background(pd.hDC, &v);
        if(HAVE_GRID      (chart_state.options)) paint_grid      (pd.hDC, &v);
        paint_chart(pd.hDC, &v);
        EndPage(pd.hDC); EndDoc(pd.hDC); 
    }

    if(pd.hDevMode)  GlobalFree(pd.hDevMode);
    if(pd.hDevNames) GlobalFree(pd.hDevNames);
    if(pd.hDC)       DeleteDC(pd.hDC);
}

/* ------------------------------------------------------------------------ */
/* Werte fuer die min. Fenstergroesse an Windows zurueckgeben.              */
/* ------------------------------------------------------------------------ */
static void min_max(MINMAXINFO *info)
{
    assert(info);
    info->ptMinTrackSize.x = MIN_WIDTH;
    info->ptMinTrackSize.y = MIN_HEIGHT;
}

/* ------------------------------------------------------------------------ */
/* Anwender hat die rechte Maustaste betaetigt: Popup-Menue anzeigen.       */
/* ------------------------------------------------------------------------ */
static void mouse_btn(HWND hwnd, LPARAM lp)
{
    int item; POINT pt;
    HMENU menu = CreatePopupMenu();

    assert(hwnd);
    assert(menu);

    AppendMenu(menu, MF_STRING, 1, _T("Zoom In"));
    AppendMenu(menu, MF_STRING, 2, _T("Zoom Out"));
    AppendMenu(menu, MF_SEPARATOR, 0, 0);
    AppendMenu(menu, MF_STRING, 3, _T("Show/Hide Grid"));
    AppendMenu(menu, MF_STRING, 4, _T("Print..."));
    AppendMenu(menu, MF_SEPARATOR, 0, 0);
    AppendMenu(menu, MF_STRING, 5, _T("Close"));

    GetCursorPos(&pt);
    item = TrackPopupMenu(menu, TPM_RETURNCMD, pt.x, pt.y, 0, hwnd, NULL);
    DestroyMenu(menu);

    switch(item)
    {
    case 1: zoom_in(hwnd, lp);                 break;
    case 2: zoom_out(hwnd);                    break;
    case 3: grid_on_off(hwnd);                 break;
    case 4: print(hwnd);                       break;
    case 5: SendMessage(hwnd, WM_CLOSE, 0, 0); break;
    }
}

/* ------------------------------------------------------------------------ */
/* Bei einem animierten Chart wird das naechste Einzelbild angezeigt.       */
/* ------------------------------------------------------------------------ */
static void next_frame(HWND hwnd)
{
    size_t last_frame_start = chart_state.current_frame_start;

    /* Naechste END_OF_FRAME-Markierung im Chart suchen... */
    while(chart_state.current_frame_start < chart_state.num_objects)
    {
        size_t tmp = chart_state.current_frame_start;
        if(chart_objects[tmp].type == END_OF_FRAME) break;  /* ...gefunden! */
        ++chart_state.current_frame_start;
    }

    /* Das naechste Einzelbild beginnt mit dem ersten Objekt nach (!)
       der soeben gefundenen Markierung. */
    ++chart_state.current_frame_start;

    /* Nach dem letzten Einzelbild beginnt die Animation wieder von vorn. */
    if(chart_state.current_frame_start >= chart_state.num_objects)
        chart_state.current_frame_start = 0;

    /* Nun wird das neue Einzelbild auf dem Bildschirm dargestellt. */
    if(last_frame_start != chart_state.current_frame_start)
        InvalidateRect(hwnd, NULL, TRUE);
}

/* ------------------------------------------------------------------------ */
/* Fenster-Ereignisse an die entsprechenden Funktionen weiterleiten.        */
/* ------------------------------------------------------------------------ */
static LRESULT CALLBACK wnd_proc(HWND hwnd, UINT msg, WPARAM wp, LPARAM lp)
{
    assert(hwnd);
    switch(msg)
    {
    case WM_CLOSE:         DestroyWindow(hwnd);              return 0;
    case WM_DESTROY:       PostQuitMessage(0);               return 0;
    case WM_LBUTTONDOWN:   zoom_in(hwnd, lp);                return 0;
    case WM_RBUTTONDOWN:   mouse_btn(hwnd, lp);              return 0;
    case WM_PAINT:         paint(hwnd);                      return 0;
    case WM_GETMINMAXINFO: min_max((MINMAXINFO*)lp);         return 0;
    case WM_SIZE:          InvalidateRect(hwnd, NULL, TRUE); return 0;
    case WM_TIMER:         next_frame(hwnd);                 return 0;
    }
    return DefWindowProc(hwnd, msg, wp, lp);
}

/* ------------------------------------------------------------------------ */
/* Aktuelle Ausgabeposition setzen.                                         */
/* ------------------------------------------------------------------------ */
void chart_moveto(double x, double y)
{
    chart_state.current_x = x;
    chart_state.current_y = y;
}

/* ------------------------------------------------------------------------ */
/* Linie von der aktuellen Ausgabeposition zum angegebenen Punkt zeichnen.  */
/* Fuer den Parameter "color" sind folgende Werte moeglich: CHART_BLACK,    */
/* CHART_RED, CHART_GREEN, CHART_BLUE und CHART_GRAY.                       */
/* ------------------------------------------------------------------------ */
void chart_lineto(double x, double y, int color)
{
    assert(chart_state.num_objects < CHART_MAX_OBJECTS);
    if(chart_state.num_objects < CHART_MAX_OBJECTS)
    {
        OBJECT *obj = &chart_objects[chart_state.num_objects++];
        obj->x1 = chart_state.current_x; obj->x2 = x;
        obj->y1 = chart_state.current_y; obj->y2 = y;
        obj->color = color;
        obj->type = LINE_OBJECT;
        chart_state.current_x = x;
        chart_state.current_y = y;
    }
}

/* ------------------------------------------------------------------------ */
/* Eine komplette Kurve aus "n" aufeinanderfolgenden x- bzw. y-Koordinaten  */
/* wird in einem Zug dargestellt. Die aktuelle Ausgabeposition befindet     */
/* sich anschliessend am letzten Stuetzpunkt der Kurve. Fuer den Parameter  */
/* "color" sind die folgenden Werte moeglich: CHART_BLACK, CHART_RED,       */
/* CHART_GREEN, CHART_BLUE und CHART_GRAY. (Der Aufrufer dieser Funktion    */
/* muss sicherstellen, dass die Parameter "x" und "y" jeweils auf den       */
/* Beginn eines Vektors mit "n" double-Werten zeigen!)                      */
/* ------------------------------------------------------------------------ */
void chart_line_series(size_t n, const double *x, const double *y, int color)
{
    size_t i;
    assert(x != NULL && y != NULL);
    for(i = 0; i < n; ++i)
    {
        if(!i) { chart_moveto(x[i], y[i]); continue; }
        assert(chart_state.num_objects < CHART_MAX_OBJECTS);
        if(chart_state.num_objects >= CHART_MAX_OBJECTS) return;
        chart_lineto(x[i], y[i], color);
    }
}

/* ------------------------------------------------------------------------ */
/* Variante von chart_line_series(), es wird allerdings nur eine Liste von  */
/* y-Koordinaten uebergeben. Die x-Koordinaten sind immer: 0, 1, 2 usw...   */
/* ------------------------------------------------------------------------ */
void chart_line_series1(size_t n, const double *y, int color)
{
    size_t i;
    assert(y != NULL);
    for(i = 0; i < n; ++i)
    {
        if(!i) { chart_moveto(0.0, y[i]); continue; }
        assert(chart_state.num_objects < CHART_MAX_OBJECTS);
        if(chart_state.num_objects >= CHART_MAX_OBJECTS) return;
        chart_lineto((double)i, y[i], color);
    }
}

/* ------------------------------------------------------------------------ */
/* Markierung am angegebenen Punkt ausgeben. Fuer den Parameter "color"     */
/* sind folgende Werte moeglich: CHART_BLACK, CHART_RED, CHART_GREEN,       */
/* CHART_BLUE und CHART_GRAY.                                               */
/* ------------------------------------------------------------------------ */
void chart_point(double x, double y, int color)
{
    assert(chart_state.num_objects < CHART_MAX_OBJECTS);
    if(chart_state.num_objects < CHART_MAX_OBJECTS)
    {
        OBJECT *obj = &chart_objects[chart_state.num_objects++];
        obj->x1 = obj->x2 = x;
        obj->y1 = obj->y2 = y;
        obj->color = color;
        obj->type = POINT_OBJECT;
    }
}

/* ------------------------------------------------------------------------ */
/* Eine Punktesequenz aus "n" aufeinanderfolgenden x- bzw. y-Koordinaten    */
/* wird in einem Zug dargestellt. Die aktuelle Ausgabeposition wird durch   */
/* den Aufruf dieser Funktion nicht veraendert. Fuer den Parameter "col"    */
/* sind die folgenden Werte moeglich: CHART_BLACK, CHART_RED, CHART_GREEN,  */
/* CHART_BLUE, CHART_GRAY. (Der Aufrufer muss sicherstellen, dass "x" und   */
/* "y" jeweils auf den Beginn eines Vektors mit "n" double-Werten zeigen!)  */
/* ------------------------------------------------------------------------ */
void chart_point_series(size_t n, const double *x, const double *y, int col)
{
    size_t i;
    assert(x != NULL && y != NULL);
    for(i = 0; i < n; ++i)
    {
        assert(chart_state.num_objects < CHART_MAX_OBJECTS);
        if(chart_state.num_objects >= CHART_MAX_OBJECTS) return;
        chart_point(x[i], y[i], col);
    }
}

/* ------------------------------------------------------------------------ */
/* Variante von chart_point_series(), es wird allerdings nur eine Liste von */
/* y-Koordinaten uebergeben. Die x-Koordinaten sind immer: 0, 1, 2 usw...   */
/* ------------------------------------------------------------------------ */
void chart_point_series1(size_t n, const double *y, int color)
{
    size_t i;
    assert(y != NULL);
    for(i = 0; i < n; ++i)
    {
        assert(chart_state.num_objects < CHART_MAX_OBJECTS);
        if(chart_state.num_objects >= CHART_MAX_OBJECTS) return;
        chart_point((double)i, y[i], color);
    }
}

/* ------------------------------------------------------------------------ */
/* Es wird ein animiertes Chart aufgebaut, das aus mehreren Einzelbildern   */
/* (sog. Frames) besteht. Der Aufruf von chart_end_of_frame() bewirkt,      */
/* dass ein weiteres Einzelbild zum Chart hinzugefuegt wird - nachfolgende  */
/* Aufrufe von chart_point(), chart_moveto() bzw. chart_lineto() beziehen   */
/* sich auf das neue Einzelbild. Die aktuelle Ausgabeposition wird durch    */
/* chart_end_of_frame() auf (0; 0) zurueckgesetzt.                          */
/*                                                                          */
/* Beispiel: Soll eine Animation erstellt werden, die aus lediglich zwei    */
/* Einzelbildern besteht, so werden zunaechst die Punkte und Linien des     */
/* ersten Einzelbildes mittels chart_point() usw. definiert. Es folgt der   */
/* Aufruf von chart_end_of_frame(). Nun werden die Punkte und Linien des    */
/* zweiten Einzelbildes definiert und schliesslich chart_show() zur Anzeige */
/* auf dem Bildschirm aufgerufen. Beide Einzelbilder werden automatisch     */
/* abwechselnd angezeigt, die Geschwindigkeit der Animation kann mittels    */
/* chart_frame_timer() eingestellt werden.                                  */
/* ------------------------------------------------------------------------ */
void chart_end_of_frame(void)
{
    assert(chart_state.num_objects < CHART_MAX_OBJECTS);
    if(chart_state.num_objects < CHART_MAX_OBJECTS)
    {
        OBJECT *obj = &chart_objects[chart_state.num_objects++];
        obj->type = END_OF_FRAME;
        obj->x1 = chart_state.current_x;
        obj->y1 = chart_state.current_y;
        chart_moveto(0, 0);
    }
}

/* ------------------------------------------------------------------------ */
/* Bei einem animierten Chart kann durch chart_frame_timer() eingestellt    */
/* werden, wie lange jedes Einzelbild (sog. Frame) auf dem Bildschirm       */
/* angezeigt wird, siehe auch: chart_end_of_frame(). Minimal koennen        */
/* 100 msec, maximal 10000 msec eingestellt werden.                         */
/* ------------------------------------------------------------------------ */
void chart_frame_timer(int msec)
{
    assert(msec >= 100 && msec <= 10000);
    if(msec >= 100 && msec <= 10000) chart_state.frame_timer_msec = msec;
}

/* ------------------------------------------------------------------------ */
/* Alle Linien und Punkte loeschen, Ausgabeposition auf (0; 0) setzen.      */
/* ------------------------------------------------------------------------ */
void chart_clear(void)
{
    chart_state.num_objects = chart_state.current_frame_start = 0;
    chart_state.zoom_factor = chart_state.zoom_x = chart_state.zoom_y = 0;
    chart_state.background_color = CHART_COLOR;

    chart_moveto(0, 0);
    chart_axis_fmt(AXIS_DEFLT_FMT, AXIS_DEFLT_FMT);
    chart_frame_timer(1000);
    chart_axis_xlimit(-AXIS_DEFLT_LIM, +AXIS_DEFLT_LIM);
    chart_axis_ylimit(-AXIS_DEFLT_LIM, +AXIS_DEFLT_LIM);
}

/* ------------------------------------------------------------------------ */
/* Anzahl der Objekte (Punkte, Linienstuecke usw.) im Chart abfragen.       */
/* ------------------------------------------------------------------------ */
size_t chart_size(void)
{
    return chart_state.num_objects;
}

/* ------------------------------------------------------------------------ */
/* Formatstrings zur Beschriftung der x- und y-Achsen einstellen,           */
/* Default = "%g". Die Formatstrings sehen aehnlich aus wie bei der         */
/* Bibliotheksfunktion "printf" (fuer Beispiele vergl. HMChart.pdf).        */
/* ------------------------------------------------------------------------ */
void chart_axis_fmt(const char *xfmt, const char *yfmt)
{
    if(!xfmt || !yfmt)
    {
        assert(!"null parameter");
        return;
    }

    if(strlen(xfmt) >= AXIS_FMT_LEN || strlen(yfmt) >= AXIS_FMT_LEN)
    {
        assert(!"format string too long");
        return;
    }

    strncpy(chart_state.axis_xfmt, xfmt, AXIS_FMT_LEN);
    strncpy(chart_state.axis_yfmt, yfmt, AXIS_FMT_LEN);
    chart_state.axis_xfmt[AXIS_FMT_LEN - 1] = 0;
    chart_state.axis_yfmt[AXIS_FMT_LEN - 1] = 0;
}

/* ------------------------------------------------------------------------ */
/* Manuelle Achsenskalierung: Minimal-/Maximalwerte fuer x-Achse angeben.   */
/* Um die manuelle Achsenskalierung fuer die x-Achse zu aktivieren, muss    */
/* beim Aufruf von "chart_show" die Option "CHART_XLIMIT" angegeben sein.   */
/* ------------------------------------------------------------------------ */
void chart_axis_xlimit(double xmin, double xmax)
{
    assert(xmax > xmin);
    if(xmax > xmin)
        chart_state.axis_xmin = xmin,
        chart_state.axis_xmax = xmax;
}

/* ------------------------------------------------------------------------ */
/* Manuelle Achsenskalierung: Minimal-/Maximalwerte fuer y-Achse angeben.   */
/* Um die manuelle Achsenskalierung fuer die y-Achse zu aktivieren, muss    */
/* beim Aufruf von "chart_show" die Option "CHART_YLIMIT" angegeben sein.   */
/* ------------------------------------------------------------------------ */
void chart_axis_ylimit(double ymin, double ymax)
{
    assert(ymax > ymin);
    if(ymax > ymin)
        chart_state.axis_ymin = ymin,
        chart_state.axis_ymax = ymax;
}

/* ------------------------------------------------------------------------ */
/* Unterschreitet die Breite bzw. Hoehe des Charts eine bestimmte Grenze,   */
/* wird die Zahl der vertikalen bzw. horizontalen Gitterlinien verringert.  */
/* Mittels "chart_grid_threshold" koennen diese Grenzen eingestellt werden. */
/* ------------------------------------------------------------------------ */
void chart_grid_threshold(int width, int height)
{
    assert(width > 0); assert(height > 0);
    chart_state.grid_width_threshold = width;
    chart_state.grid_heigth_threshold = height;
}

/* ------------------------------------------------------------------------ */
/* Hintergrundfarbe des Charts einstellen (Standardfarbe = Hellgrau).       */
/* In den Parametern r, g und b werden die Rot-, Gruen- und Blau-Anteile    */
/* der gewuenschten Farbe uebergeben (jeweils im Bereich 0...255).          */
/* ------------------------------------------------------------------------ */
void chart_background(unsigned char r, unsigned char g, unsigned char b)
{
    chart_state.background_color = RGB(r, g, b);
}

/* ------------------------------------------------------------------------ */
/* Chart anzeigen und warten, bis der Anwender das Fenster schliesst.       */
/*                                                                          */
/* Fuer den Parameter "options" sind die Werte CHART_DEFAULT,               */
/* CHART_XLIMIT, CHART_YLIMIT, CHART_NOBACKGROUND, CHART_NOGRID,            */
/* CHART_XZOOM_OFF, CHART_YZOOM_OFF und CHART_NOCLEAR erlaubt.              */
/* Mehrere Optionen koennen mit dem |-Operator kombiniert werden:           */
/*                                                                          */
/* - So wird bspw. durch (CHART_NOGRID | CHART_NOBACKGROUND) ein            */
/*   Chart ohne Hintergrund und Koordinatensystem angezeigt.                */
/* - Mittels CHART_XLIMIT und CHART_YLIMIT kann die automatische            */
/*   Achsenskalierung deaktiviert werden; der darstellbare Bereich wird     */
/*   dann ueber chart_axis_xlimit() und chart_axis_ylimit() eingestellt.    */
/* - Mittels CHART_XZOOM_OFF und CHART_YZOOM_OFF kann die                   */
/*   Zoom-Funktionalitaet des Charts in X- oder Y-Richtung oder             */
/*   auch vollstaendig deaktiviert werden. Zum vollstaendigen Deaktivieren  */
/*   ist (CHART_XZOOM_OFF | CHART_YZOOM_OFF) an chart_show() zu uebergeben. */
/* - Wenn chart_show() mit der Option CHART_NOCLEAR aufgerufen wird,        */
/*   werden die Chart-Objekte (Linien, Punkte usw.) nach dem Schliessen     */
/*   des Charts nicht geloescht. Zum "manuellen Loeschen" muss dann         */
/*   ggf. die Funktion chart_clear() aufgerufen werden.                     */
/* ------------------------------------------------------------------------ */
void chart_show(int options, const char *title)
{
    HINSTANCE hInstance = get_instance();
    WNDCLASS wc; HWND hwnd;
    ATOM ok; MSG msg;
    int message_ret;

    ZeroMemory(&wc, sizeof(wc));
    wc.lpfnWndProc   = wnd_proc;
    wc.hInstance     = hInstance;
    wc.hIcon         = LoadIcon(NULL, IDI_APPLICATION);
    wc.hCursor       = LoadCursor(NULL, IDC_CROSS);
    wc.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
    wc.lpszClassName = _T(WINDOW_CLASSNAME);

    ok = RegisterClass(&wc);
    if(!ok) return;  /* Chart bereits offen? */

    hwnd = CreateWindowEx(
        WS_EX_CLIENTEDGE, _T(WINDOW_CLASSNAME), NULL, WS_OVERLAPPEDWINDOW,
        CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT,
        NULL, NULL, hInstance, NULL);
    assert(hwnd);

    chart_state.options = options;
    SetWindowTextA(hwnd, (title ? title : DEFAULT_TITLE));
    ShowWindow(hwnd, SW_SHOW);
    UpdateWindow(hwnd);

    /* Bei animierten Charts sorgt ein Timer fuer den Frame-Wechsel */
    SetTimer(hwnd, 0, chart_state.frame_timer_msec, 0);

    /* Hauptschleife: Chart anzeigen, bis Anwender das Fenster schliesst */
    while((message_ret = GetMessage(&msg, NULL, 0, 0)) > 0)
        TranslateMessage(&msg), DispatchMessage(&msg);
    assert(message_ret != -1);  /* GetMessage() liefert Fehler? */
    UnregisterClass(_T(WINDOW_CLASSNAME), hInstance);

    /* 2020-07-27: Chart-Inhalt wird beim Beenden geloescht. */
    if((~options) & CHART_NOCLEAR) chart_clear();
}

/* ------------------------------------------------------------------------ */
/* Mit diesen "xxx-Funktionen" koennen HMCharts in Benutzeroberflaechen     */
/* unter dem Betriebssystem Microsoft Windows integriert werden anstatt     */
/* sie - wie normalerweise ueblich - in separaten Fenstern zu oeffnen.      */
/* Siehe PDF-Dokumentation fuer weitere Details und ein Beispielprogramm.   */
/*                                                                          */
/* Die Deklarationen der "xxx-Funktionen" sind nicht in chart.h enthalten   */
/* und muessen daher bei Bedarf "manuell" zum Code hinzugefuegt werden!     */
/* ------------------------------------------------------------------------ */

/*
extern "C"
{
void xxx_chart_paint_hdc(RECT rc, HDC hdc);
void xxx_chart_set_options(int options);
void xxx_chart_lbutton(HWND hwnd, int mouse_x, int mouse_y);
void xxx_chart_rbutton(HWND hwnd, int mouse_x, int mouse_y);
void xxx_chart_next_frame(HWND hwnd);
}
*/

/* Chart auf Device-Context ausgeben */
void xxx_chart_paint_hdc(RECT rc, HDC hdc)
{
    paint_hdc(rc, hdc);
}

/* Chart-Optionen einstellen */
void xxx_chart_set_options(int options)
{
    chart_state.options = options;
}

/* Auf linke Maustaste reagieren */
void xxx_chart_lbutton(HWND hwnd, int mouse_x, int mouse_y)
{
    LPARAM lp = ((LPARAM)mouse_y << 16) + (LPARAM)mouse_x;
    zoom_in(hwnd, lp);
}

/* Auf rechte Maustaste reagieren */
void xxx_chart_rbutton(HWND hwnd, int mouse_x, int mouse_y)
{
    LPARAM lp = ((LPARAM)mouse_y << 16) + (LPARAM)mouse_x;
    mouse_btn(hwnd, lp);
}

/* Naechstes Teilbild ausgeben (bei animierten Charts) */
void xxx_chart_next_frame(HWND hwnd)
{
    next_frame(hwnd);
}
