/* ######################################################################## */
/* ###  USE THIS FILE "CHART-X11.C" FOR LINUX AND MAC OSX APPLICATIONS! ### */
/* ###  DIESE DATEI "CHART-X11.C" NUR FUER LINUX UND MAC OSX VERWENDEN! ### */
/* ######################################################################## */

/* ------------------------------------------------------------------------ */
/* chart-x11.c - Ausgabe von 2D-Charts unter Linux u. dem X11 Window System */
/*                                                                          */
/*                  Copyright Tilman Kuepper 2020.                          */
/*    Distributed under the Boost Software License, Version 1.0.            */
/*        (See accompanying file LICENSE_1_0.txt or copy at                 */
/*               http://www.boost.org/LICENSE_1_0.txt)                      */
/*                                                                          */
/* Letzte Bearbeitung: 27. Juli 2020                                        */
/* ------------------------------------------------------------------------ */

/* Darauf achten,  dass bei UNICODE-Projekten sowohl */
/* UNICODE als auch _UNICODE zugleich definiert sind */
#ifdef UNICODE
#undef _UNICODE
#define _UNICODE
#endif

#ifdef _UNICODE
#undef UNICODE
#define UNICODE
#endif

#include <math.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "chart.h"

/* Spezielle Headerdateien fuer X11-Version des Charts... */
#include <time.h>      /* nanosleep    */
#include <sys/time.h>  /* gettimeofday */
#include <X11/Xlib.h>
#include <X11/Xutil.h>

/* Achtung: Unterschied in Xlib.h zwischen C- und C++! */
#if defined(__cplusplus) || defined(c_plusplus)
#define VISUAL_CLASS c_class
#else
#define VISUAL_CLASS class
#endif

/* Max. Anzahl der Objekte im Chart: Linien, Punkte usw... */
#ifndef CHART_MAX_OBJECTS
#define CHART_MAX_OBJECTS 100000
#endif

#undef MIN_WIDTH
#undef MIN_HEIGHT
#undef GRID_WIDTH_THR
#undef GRID_HEIGTH_THR
#undef ZOOM_STEP
#undef ZOOM_MAX
#undef SERIES_WIDTH
#undef MIN_PT_SIZE
#undef TEXT_BUFSIZE
#undef CHECK_OVERLAP
#undef AXIS_FMT_LEN
#undef AXIS_DEFLT_FMT
#undef AXIS_DEFLT_LIM
#undef DEFAULT_WIDTH
#undef DEFAULT_HEIGHT
#undef CHART_COLOR
#undef GRID_COLOR
#undef BLACK_COLOR
#undef RED_COLOR
#undef GREEN_COLOR
#undef BLUE_COLOR
#undef GRAY_COLOR
#undef DEFAULT_TITLE
#undef MAX
#undef MIN

#define MIN_WIDTH        400          /* Min. Fensterbreite                 */
#define MIN_HEIGHT       200          /* Min. Fensterhoehe                  */
#define GRID_WIDTH_THR   800          /* Ab dieser Fenstergroesse gibt es   */
#define GRID_HEIGTH_THR  400          /* mehr Gitterlinien (Defaultwert)    */
#define ZOOM_STEP        1.25         /* Zoomfaktor-Aenderung bei Mausklick */
#define ZOOM_MAX         64.0         /* Max. Zoomfaktor                    */
#define SERIES_WIDTH     2            /* Linienbreite fuer chart_lineto()   */
#define MIN_PT_SIZE      2            /* Markierungen bei chart_point()     */
#define TEXT_BUFSIZE     1024         /* Textbuffer fuer Achsenbeschriftung */
#define CHECK_OVERLAP    50           /* "Kollision" von x/y-Labels pruefen */
#define AXIS_FMT_LEN     128          /* Formatstr. zur Achsenbeschriftung  */
#define AXIS_DEFLT_FMT   "%g"         /* Standardformat fuer x-/y-Achse     */
#define AXIS_DEFLT_LIM   1.0          /* Fuer chart_axis_xlimit()/_ylimit() */
#define DEFAULT_WIDTH   1024          /* Fensterbreite zu Beginn            */
#define DEFAULT_HEIGHT   768          /* Fensterhoehe zu Beginn             */

#define CHART_COLOR      RGB(235, 235, 235)       /* Hintergrundfarbe       */
#define GRID_COLOR       RGB(150, 150, 150)       /* Farbe der Gitterlinien */
#define BLACK_COLOR      RGB(  0,   0,   0)       /* Schwarze Linienfarbe   */
#define RED_COLOR        RGB(255,   0,   0)       /* Rote Linienfarbe       */
#define GREEN_COLOR      RGB(  0, 255,   0)       /* Gruene Linienfarbe     */
#define BLUE_COLOR       RGB(  0,   0, 255)       /* Blaue Linienfarbe      */
#define GRAY_COLOR       RGB(222, 222, 222)       /* Graue Linienfarbe      */
#define DEFAULT_TITLE    "Chart"                  /* Siehe chart_show()...  */
#define MAX(a, b)        ((a)>(b)?(a):(b))        /* Maximum bestimmen      */
#define MIN(a, b)        ((a)<(b)?(a):(b))        /* Minimum bestimmen      */

/* ------------------------------------------------------------------------ */
/* Sollen Hintergrund und/oder Koordinatensystem dargestellt werden?        */
/* ------------------------------------------------------------------------ */
#undef HAVE_BACKGROUND
#undef HAVE_GRID

#define HAVE_BACKGROUND(opt) (((opt) & CHART_NOBACKGROUND) == 0)
#define HAVE_GRID(opt)       (((opt) & CHART_NOGRID      ) == 0)

/* ------------------------------------------------------------------------ */
/* RGB-Werte in unsigned-int-Wert umrechnen, der dann an XSetForeground()   */
/* uebergeben werden kann (geht aber nur auf TrueColor-Displays...)         */
/* ------------------------------------------------------------------------ */
#undef RGB
#define RGB(r, g, b) \
    (((unsigned)(r) << 16) + ((unsigned)(g) << 8) + (unsigned)(b))

/* ------------------------------------------------------------------------ */
/* Struktur "RECT" wie bei Microsoft Windows.                               */
/* ------------------------------------------------------------------------ */
typedef struct tagRECT
{
    int right, left, top, bottom;
} RECT;

/* ------------------------------------------------------------------------ */
/* Struktur zum Speichern der min./max. Weltkoordinaten der Objekte und     */
/* der Chart-Abmessungen (in Pixeln); wird von setup_viewport() gefuellt.   */
/* ------------------------------------------------------------------------ */
typedef struct tagCHART_VIEWPORT
{
    double world_x1, world_y1, world_x2, world_y2;
    int    chart_x1, chart_y1, chart_x2, chart_y2;
    double world_width, world_height;
    int    chart_width, chart_height;
} CHART_VIEWPORT;

/* ------------------------------------------------------------------------ */
/* Im Chart befinden sich Punkte und Linien, die ggf. auf mehrere           */
/* Einzelbilder verteilt sind. Die Daten dieser "Chart-Objekte" werden      */
/* in der folgenden Struktur erfasst.                                       */
/* ------------------------------------------------------------------------ */
enum { POINT_OBJECT, LINE_OBJECT, END_OF_FRAME };
typedef struct tagOBJECT
{
    int type;  /* POINT_OBJECT, LINE_OBJECT oder END_OF_FRAME */
    double x1, y1, x2, y2;
    int color;
} OBJECT;

/* Hier ist der komplette Chart-Inhalt gespeichert. */
static OBJECT chart_objects[CHART_MAX_OBJECTS];

/* ------------------------------------------------------------------------ */
/* Struktur mit aktueller Objekt-Anzahl, dem aktuellem Zoom-Zustand und     */
/* den gewuenschten Darstellungsoptionen (mit/ohne Koordinatensystem).      */
/* ------------------------------------------------------------------------ */
typedef struct tagCHART_DATA
{
    size_t num_objects, current_frame_start;
    double current_x, current_y;
    double zoom_factor, zoom_x, zoom_y;
    char axis_xfmt[AXIS_FMT_LEN], axis_yfmt[AXIS_FMT_LEN];
    int options, frame_timer_msec;
    double axis_xmin, axis_xmax, axis_ymin, axis_ymax;
    int grid_width_threshold, grid_heigth_threshold;
    unsigned int background_color;
} CHART_DATA;

/* Hier ist der aktuelle Chart-Zustand gespeichert. */
static CHART_DATA chart_state =
{
    0, 0,                               /* num_objects, current_frame_start */
    0.0, 0.0,                           /* current_x, current_y             */
    0.0, 0.0, 0.0,                      /* zoom_factor, zoom_x, zoom_y      */
    AXIS_DEFLT_FMT, AXIS_DEFLT_FMT,     /* axis_xfmt, axis_yfmt             */
    CHART_DEFAULT, 1000,                /* options, frame_timer_msec        */
    -AXIS_DEFLT_LIM, +AXIS_DEFLT_LIM,   /* axis_xmin, axis_xmax             */
    -AXIS_DEFLT_LIM, +AXIS_DEFLT_LIM,   /* axis_ymin, axis_ymax             */
    GRID_WIDTH_THR, GRID_HEIGTH_THR,    /* grid_width/heigth_threshold      */
    CHART_COLOR                         /* background_color                 */
};

/* ------------------------------------------------------------------------ */
/* Zeitspanne in msec zwischen zwei gegebenen Zeitpunkten ermitteln         */
/* ------------------------------------------------------------------------ */
static int diff_msec(struct timeval *start, struct timeval *end)
{
    struct timeval tmp;
    tmp.tv_sec  = end->tv_sec  - start->tv_sec;
    tmp.tv_usec = end->tv_usec - start->tv_usec;
    if(tmp.tv_usec < 0) --tmp.tv_sec, tmp.tv_usec += 1000000;
    return (int)tmp.tv_sec * 1000 + (int)(tmp.tv_usec / 1000);
}

/* ------------------------------------------------------------------------ */
/* Vordergrundfarbe zum Zeichnen von Linien und Texten setzen               */
/* ------------------------------------------------------------------------ */
static void SetColor(Display *d, unsigned int color)
{
    GC gc;
    assert(d);

    gc = DefaultGC(d, DefaultScreen(d));
    XSetForeground(d, gc, color);
}

/*
Die bisherige Version von SetColor() basiert auf XAllocNamedColor(),
allerdings - faelschlicherweise? - ohne die angelegten Farben durch
einen Aufruf von XFreeColors() wieder freizugeben. Da auf TrueColor-
Systemen RGB-Werte auch direkt an XSetForeground() uebergeben werden
koennen, wird HMChart ab 04.06.2017 auf TrueColor-Farben umgestellt.
Andere Systeme werden von nun an nicht mehr unterstuetzt!

static void SetColor(Display *d, const char *color_name)
{
    int ok;
    XColor near_color, true_color;
    Colormap cmap;
    GC gc;

    assert(d);
    assert(color_name);

    gc = DefaultGC(d, DefaultScreen(d));
    assert(gc);

    cmap = DefaultColormap(d, 0);
    assert(cmap);

    ok = XAllocNamedColor(d, cmap, color_name, &near_color, &true_color);
    assert(ok);

    ok = XSetForeground(d, gc, BlackPixel(d, 0) ^ near_color.pixel);
    assert(ok);    
}
*/

/* ------------------------------------------------------------------------ */
/* Die min./max. Weltkoordinaten der Punkte und Linien im Chart sowie die   */
/* Chart-Abmessungen in Pixeln werden ermittelt und in der uebergebenen     */
/* CHART_VIEWPORT-Struktor abgelegt. Diese Funktion wird zu Beginn der      */
/* Ausgabe auf dem Bildschirm oder Drucker aufgerufen.                      */
/* ------------------------------------------------------------------------ */
static void setup_viewport(const RECT *rc, CHART_VIEWPORT *v)
{
    size_t i;

    assert(rc);
    assert(v);

    /* Abmessungen des Charts in Pixeln */
    v->chart_x1 = rc->left; v->chart_x2 = rc->right;
    v->chart_y1 = rc->top;  v->chart_y2 = rc->bottom;

    /* Suche nach den min./max. Weltkoordinaten */
    v->world_x1 = chart_state.num_objects ? chart_objects[0].x1 : 0;
    v->world_y1 = chart_state.num_objects ? chart_objects[0].y1 : 0;
    v->world_x2 = v->world_x1; v->world_y2 = v->world_y1;

    for(i = 0; i < chart_state.num_objects; ++i)
    {
        const OBJECT *obj = &chart_objects[i];
        if(obj->type == END_OF_FRAME) continue;

        v->world_x1 = MIN(obj->x1, v->world_x1);
        v->world_x2 = MAX(obj->x1, v->world_x2);
        v->world_y1 = MIN(obj->y1, v->world_y1);
        v->world_y2 = MAX(obj->y1, v->world_y2);

        if(obj->type != LINE_OBJECT) continue;
        v->world_x1 = MIN(obj->x2, v->world_x1);
        v->world_x2 = MAX(obj->x2, v->world_x2);
        v->world_y1 = MIN(obj->y2, v->world_y1);
        v->world_y2 = MAX(obj->y2, v->world_y2);
    }

    /* Wurden die Achsen evtl. manuell eingestellt? */
    if(chart_state.options & CHART_XLIMIT)
        v->world_x1 = chart_state.axis_xmin, v->world_x2 = chart_state.axis_xmax;
    if(chart_state.options & CHART_YLIMIT)
        v->world_y1 = chart_state.axis_ymin, v->world_y2 = chart_state.axis_ymax;

    /* Aktuellen Zoom-Faktor beruecksichtigen */
    if(chart_state.zoom_factor > 1 && !(chart_state.options & CHART_XZOOM_OFF))
    {
        double world_width;
        world_width = (v->world_x2 - v->world_x1) / chart_state.zoom_factor;
        v->world_x1 = chart_state.zoom_x - world_width / 2.0;
        v->world_x2 = chart_state.zoom_x + world_width / 2.0;
    }
    if(chart_state.zoom_factor > 1 && !(chart_state.options & CHART_YZOOM_OFF))
    {
        double world_height;
        world_height = (v->world_y2 - v->world_y1) / chart_state.zoom_factor;
        v->world_y1 = chart_state.zoom_y - world_height / 2.0;
        v->world_y2 = chart_state.zoom_y + world_height / 2.0;
    }

    /* Sonderbehandlung fuer Achsenbeschriftung bei leeren Charts */
    if(v->world_x1 == v->world_x2) (v->world_x1 -= 1), (v->world_x2 += 1);
    if(v->world_y1 == v->world_y2) (v->world_y1 -= 1), (v->world_y2 += 1);

    v->world_width  = v->world_x2 - v->world_x1;
    v->world_height = v->world_y2 - v->world_y1;
    v->chart_width  = v->chart_x2 - v->chart_x1;
    v->chart_height = v->chart_y2 - v->chart_y1;
}

/* ------------------------------------------------------------------------ */
/* X-Weltkoordinate in X-Position (in Pixeln) umrechnen.                    */
/* ------------------------------------------------------------------------ */
static int x_to_screen(const CHART_VIEWPORT *v, double world_x)
{
    double factor = (world_x - v->world_x1) / v->world_width;
    return v->chart_x1 + (int)(factor * v->chart_width);
}

/* ------------------------------------------------------------------------ */
/* Y-Weltkoordinate in Y-Position (in Pixeln) umrechnen.                    */
/* ------------------------------------------------------------------------ */
static int y_to_screen(const CHART_VIEWPORT *v, double world_y)
{
    double factor = (world_y - v->world_y1) / v->world_height;
    return v->chart_y2 - (int)(factor * v->chart_height);
}

/* ------------------------------------------------------------------------ */
/* X-Position (in Pixeln) in X-Weltkoordinate umrechnen.                    */
/* ------------------------------------------------------------------------ */
static double screen_to_x(const CHART_VIEWPORT *v, int screen_x)
{
    double factor = 1.0 * (screen_x - v->chart_x1) / v->chart_width;
    return v->world_x1 + (factor * v->world_width);
}

/* ------------------------------------------------------------------------ */
/* Y-Position (in Pixeln) in Y-Weltkoordinate umrechnen.                    */
/* ------------------------------------------------------------------------ */
static double screen_to_y(const CHART_VIEWPORT *v, int screen_y)
{
    double factor = 1.0 * (screen_y - v->chart_y1) / v->chart_height;
    return v->world_y2 - (factor * v->world_height);
}

/* ------------------------------------------------------------------------ */
/* Der Menuepunkt "Show/Hide Grid" (Taste "G") wurde gewaehlt. Es werden    */
/* die Hintergrundfarbe und die Gitterlinien ein- bzw. ausgeschaltet.       */
/* ------------------------------------------------------------------------ */
static void grid_on_off(void)
{
    if(HAVE_GRID(chart_state.options))
        chart_state.options += CHART_NOGRID;
    else if(HAVE_BACKGROUND(chart_state.options))
        chart_state.options -= CHART_NOGRID,
        chart_state.options += CHART_NOBACKGROUND;
    else
        chart_state.options -= CHART_NOGRID,
        chart_state.options -= CHART_NOBACKGROUND;
}

/* ------------------------------------------------------------------------ */
/* Linke Maustaste gedrueckt: Ansicht wird um eine Stufe vergroessert.      */
/* ------------------------------------------------------------------------ */
static void zoom_in(Display *d, Window w)
{
    XWindowAttributes windowattr;
    int rx, ry, wx, wy, ok;
    unsigned int mask;
    Window root, child;
    CHART_VIEWPORT v;
    RECT rc;

    /* Funktion abbrechen, wenn Zoom-Funktion komplett deaktiviert ist */
    const int options = chart_state.options;
    if((options & CHART_XZOOM_OFF) && (options & CHART_YZOOM_OFF)) return;

    assert(d);
    ok = XQueryPointer(d, w, &root, &child, &rx, &ry, &wx, &wy, &mask);
    assert(ok);

    ok = XGetWindowAttributes(d, w, &windowattr);
    assert(ok);
    
    rc.left   = rc.top = 0;
    rc.right  = windowattr.width;
    rc.bottom = windowattr.height;

    setup_viewport(&rc, &v);
    chart_state.zoom_x = screen_to_x(&v, wx);
    chart_state.zoom_y = screen_to_y(&v, wy);

    chart_state.zoom_factor *= ZOOM_STEP;
    if(chart_state.zoom_factor > ZOOM_MAX) XBell(d, 50);
    chart_state.zoom_factor = MAX(chart_state.zoom_factor, ZOOM_STEP);
    chart_state.zoom_factor = MIN(chart_state.zoom_factor, ZOOM_MAX );
}

/* ------------------------------------------------------------------------ */
/* Urspruengliche Ansicht bzw. Vergroesserungsstufe wiederherstellen.       */
/* ------------------------------------------------------------------------ */
static void zoom_out(void)
{
    chart_state.zoom_factor = 0;
}

/* ------------------------------------------------------------------------ */
/* Farbigen Hintergrund ausgeben                                            */
/* ------------------------------------------------------------------------ */
static void paint_background(Display *d, Window w, const CHART_VIEWPORT *v)
{
    GC gc = DefaultGC(d, DefaultScreen(d));
    unsigned int width  = (unsigned int)(v->chart_x2 - v->chart_x1);
    unsigned int height = (unsigned int)(v->chart_y2 - v->chart_y1);
    assert(v->chart_x2 > v->chart_x1);
    assert(v->chart_y2 > v->chart_y1);

    SetColor(d, chart_state.background_color);
    XFillRectangle(d, w, gc, 0, 0, width, height);
}

/* ------------------------------------------------------------------------ */
/* Koordinatensystem ausgeben.                                              */
/* ------------------------------------------------------------------------ */
static void paint_grid(Display *d, Window w, const CHART_VIEWPORT *v)
{
    char text[TEXT_BUFSIZE];
    double x, y, exp10, xstart, xwidth, xstep, ystart, ywidth, ystep;
    int x_scr = 0, y_scr = 0, ok, first_y_scr;
    GC gc = DefaultGC(d, DefaultScreen(d));
    ok = XSetLineAttributes(d, gc, 1, LineOnOffDash, CapRound, JoinRound);
    assert(ok);

    /* Welcher x-Bereich muss dargestellt werden? */
    xwidth = v->world_width, exp10 = 0;
    while(xwidth <  1) xwidth *= 10, --exp10;
    while(xwidth > 10) xwidth /= 10, ++exp10;

    /* Bei kleinen Fenstern -> weniger Gitterlinien zeichnen. */
    if(v->chart_width < chart_state.grid_width_threshold) xwidth *= 2.0;

    /* Schrittweite der x-Gitterlinien ermitteln */
    if     (xwidth > 8.0) xstep = 2.0 * pow(10, exp10);
    else if(xwidth > 4.0) xstep = 1.0 * pow(10, exp10);
    else if(xwidth > 2.0) xstep = 0.5 * pow(10, exp10);
    else                  xstep = 0.2 * pow(10, exp10);

    /* Position/Koordinate der ersten x-Gitterlinie ermitteln. */
    xstart = floor(v->world_x1 / pow(10, exp10 + 1)) * pow(10, exp10 + 1);
    if(v->world_x1 * v->world_x2 <= 0) xstart = 0;
    while(xstart  > v->world_x1) xstart -= xstep;
    while(xstart <= v->world_x1) xstart += xstep;

    /* Welcher y-Bereich muss dargestellt werden? */
    ywidth = v->world_height, exp10 = 0;
    while(ywidth <  1) ywidth *= 10, --exp10;
    while(ywidth > 10) ywidth /= 10, ++exp10;

    /* Bei kleinen Fenstern -> weniger Gitterlinien zeichnen. */
    if(v->chart_height < chart_state.grid_heigth_threshold) ywidth *= 2.0;

    /* Schrittweite der y-Gitterlinien ermitteln */
    if     (ywidth > 6.0) ystep = 2.0 * pow(10, exp10);
    else if(ywidth > 3.0) ystep = 1.0 * pow(10, exp10);
    else if(ywidth > 1.5) ystep = 0.5 * pow(10, exp10);
    else                  ystep = 0.2 * pow(10, exp10);

    /* Position/Koordinate der ersten y-Gitterlinie ermitteln. */
    ystart = floor(v->world_y1 / pow(10, exp10 + 1)) * pow(10, exp10 + 1);
    if(v->world_y1 * v->world_y2 <= 0) ystart = 0;
    while(ystart >  v->world_y1) ystart -= ystep;
    while(ystart <= v->world_y1) ystart += ystep;

    /* y-Beschriftungen ausgeben */
    SetColor(d, BLACK_COLOR);
    first_y_scr = y_to_screen(v, ystart);
    for(y = ystart; y < v->world_y2; y += ystep)
    {
        /* Trick, damit Null-Linie nicht mit "1.3e-16" beschriftet wird. */
        if(fabs(y) < v->world_height / 1000.0) y = 0;
        sprintf(text, chart_state.axis_yfmt, y);
        x_scr = x_to_screen(v, v->world_x1), y_scr = y_to_screen(v, y);
        XDrawString(d, w, DefaultGC(d, DefaultScreen(d)), 
            2 + x_scr, y_scr - 2, text, (int)strlen(text));
    }

    /* x-Beschriftungen ausgeben */
    for(x = xstart; x < v->world_x2; x += xstep)
    {
        /* x-Werte nicht ausgeben, wenn die Gefahr besteht, dass sich
         * x- und y-Werte in der Fensterecke ueberschneiden. */
        x_scr = x_to_screen(v, x), y_scr = y_to_screen(v, v->world_y1);
        if(x_scr < CHECK_OVERLAP)
           if(first_y_scr + CHECK_OVERLAP > v->chart_height) continue;

        /* Trick, damit Null-Linie nicht mit "1.3e-16" beschriftet wird. */
        if(fabs(x) < v->world_width  / 1000.0) x = 0;
        sprintf(text, chart_state.axis_xfmt, x);
        XDrawString(d, w, DefaultGC(d, DefaultScreen(d)), 
            2 + x_scr, y_scr - 2, text, (int)strlen(text));
    }

    /* x-Gitterlinien zeichnen */
    SetColor(d, GRID_COLOR);
    for(x = xstart; x < v->world_x2; x += xstep)
    {
        XDrawLine(d, w, DefaultGC(d, DefaultScreen(d)),
            x_to_screen(v, x), y_to_screen(v, v->world_y1),
            x_to_screen(v, x), y_to_screen(v, v->world_y2));
    }

    /* y-Gitterlinien zeichnen */
    for(y = ystart; y < v->world_y2; y += ystep)
    {
        XDrawLine(d, w, DefaultGC(d, DefaultScreen(d)),
            x_to_screen(v, v->world_x1), y_to_screen(v, y),
            x_to_screen(v, v->world_x2), y_to_screen(v, y));
    }
}

/* ------------------------------------------------------------------------ */
/* Punkte und Linien ausgeben.                                              */
/* ------------------------------------------------------------------------ */
static void paint_chart(Display *d, Window w, const CHART_VIEWPORT *v)
{
    size_t i;
    int x1, x2, y1, y2, color = CHART_BLACK;
    const unsigned int width = SERIES_WIDTH;
    unsigned int pnt_width, pnt_height;

    GC gc = DefaultGC(d, DefaultScreen(d));
    int ok = XSetLineAttributes(d, gc, width, LineSolid, CapRound, JoinRound);
    assert(ok);

    /* Die Chart-Objekte werden der Reihe nach durchlaufen und ausgeben.
       Die Ausgabe wird abgebrochen, falls das Ende der Objektliste erreicht
       wurde oder - bei einem animierten Chart - wenn das aktuelle Einzelbild
       vollstaendig ausgegeben wurde. */
    SetColor(d, BLACK_COLOR);
    for(i = chart_state.current_frame_start; i < chart_state.num_objects; ++i)
    {
        const OBJECT *obj = &chart_objects[i];
        if(obj->type == END_OF_FRAME) break;  /* Ende des Einzelbildes! */

        if(color != obj->color)
        {
            switch(obj->color)
            {
            case CHART_BLACK: SetColor(d, BLACK_COLOR); break;
            case CHART_RED:   SetColor(d, RED_COLOR  ); break;
            case CHART_GREEN: SetColor(d, GREEN_COLOR); break;
            case CHART_BLUE:  SetColor(d, BLUE_COLOR ); break;
            case CHART_GRAY:  SetColor(d, GRAY_COLOR ); break;
            default: assert(!"invalid color");
            }
            color = obj->color;
        }

        switch(obj->type)
        {
        case LINE_OBJECT:
            x1 = x_to_screen(v, obj->x1); y1 = y_to_screen(v, obj->y1);
            x2 = x_to_screen(v, obj->x2); y2 = y_to_screen(v, obj->y2);
            XDrawLine(d, w, gc, x1, y1, x2, y2);
            break;
        case POINT_OBJECT:
            x1 = x_to_screen(v, obj->x1) - MIN_PT_SIZE;
            y1 = y_to_screen(v, obj->y1) - MIN_PT_SIZE;
            pnt_width = pnt_height = 2 * MIN_PT_SIZE;
            XDrawRectangle(d, w, gc, x1, y1, pnt_width, pnt_height);
            break;
        default:
            assert(!"invalid object type");
        }
    }
}

/* ------------------------------------------------------------------------ */
/* Chart (inkl. Hintergrund) auf Bildschirm ausgeben.                       */
/* ------------------------------------------------------------------------ */
static void paint(Display *d, Window w)
{
    XWindowAttributes windowattr;
    CHART_VIEWPORT v;
    RECT rc;
    
    int ok = XGetWindowAttributes(d, w, &windowattr);
    assert(ok);
    
    rc.left   = rc.top = 0;
    rc.right  = windowattr.width;
    rc.bottom = windowattr.height;
    setup_viewport(&rc, &v);

    if(HAVE_BACKGROUND(chart_state.options))
        paint_background(d, w, &v);
    else
        XClearWindow(d, w);

    if(HAVE_GRID(chart_state.options)) paint_grid(d, w, &v);
    paint_chart(d, w, &v);
}

/* ------------------------------------------------------------------------ */
/* Bei einem animierten Chart wird das naechste Einzelbild angezeigt.       */
/* ------------------------------------------------------------------------ */
static void next_frame(Display *d, Window w)
{
    size_t last_frame_start = chart_state.current_frame_start;

    /* Naechste END_OF_FRAME-Markierung im Chart suchen... */
    while(chart_state.current_frame_start < chart_state.num_objects)
    {
        size_t tmp = chart_state.current_frame_start;
        if(chart_objects[tmp].type == END_OF_FRAME) break;  /* ...gefunden! */
        ++chart_state.current_frame_start;
    }

    /* Das naechste Einzelbild beginnt mit dem ersten Objekt nach (!)
       der soeben gefundenen Markierung. */
    ++chart_state.current_frame_start;

    /* Nach dem letzten Einzelbild beginnt die Animation wieder von vorn. */
    if(chart_state.current_frame_start >= chart_state.num_objects)
        chart_state.current_frame_start = 0;

    /* Nun wird das neue Einzelbild auf dem Bildschirm dargestellt. */
    if(last_frame_start != chart_state.current_frame_start)
        paint(d, w);
}

/* ------------------------------------------------------------------------ */
/* Aktuelle Ausgabeposition setzen.                                         */
/* ------------------------------------------------------------------------ */
void chart_moveto(double x, double y)
{
    chart_state.current_x = x;
    chart_state.current_y = y;
}

/* ------------------------------------------------------------------------ */
/* Linie von der aktuellen Ausgabeposition zum angegebenen Punkt zeichnen.  */
/* Fuer den Parameter "color" sind folgende Werte moeglich: CHART_BLACK,    */
/* CHART_RED, CHART_GREEN, CHART_BLUE und CHART_GRAY.                       */
/* ------------------------------------------------------------------------ */
void chart_lineto(double x, double y, int color)
{
    assert(chart_state.num_objects < CHART_MAX_OBJECTS);
    if(chart_state.num_objects < CHART_MAX_OBJECTS)
    {
        OBJECT *obj = &chart_objects[chart_state.num_objects++];
        obj->x1 = chart_state.current_x; obj->x2 = x;
        obj->y1 = chart_state.current_y; obj->y2 = y;
        obj->color = color;
        obj->type = LINE_OBJECT;
        chart_state.current_x = x;
        chart_state.current_y = y;
    }
}

/* ------------------------------------------------------------------------ */
/* Eine komplette Kurve aus "n" aufeinanderfolgenden x- bzw. y-Koordinaten  */
/* wird in einem Zug dargestellt. Die aktuelle Ausgabeposition befindet     */
/* sich anschliessend am letzten Stuetzpunkt der Kurve. Fuer den Parameter  */
/* "color" sind die folgenden Werte moeglich: CHART_BLACK, CHART_RED,       */
/* CHART_GREEN, CHART_BLUE und CHART_GRAY. (Der Aufrufer dieser Funktion    */
/* muss sicherstellen, dass die Parameter "x" und "y" jeweils auf den       */
/* Beginn eines Vektors mit "n" double-Werten zeigen!)                      */
/* ------------------------------------------------------------------------ */
void chart_line_series(size_t n, const double *x, const double *y, int color)
{
    size_t i;
    assert(x != NULL && y != NULL);
    for(i = 0; i < n; ++i)
    {
        if(!i) { chart_moveto(x[i], y[i]); continue; }
        assert(chart_state.num_objects < CHART_MAX_OBJECTS);
        if(chart_state.num_objects >= CHART_MAX_OBJECTS) return;
        chart_lineto(x[i], y[i], color);
    }
}

/* ------------------------------------------------------------------------ */
/* Variante von chart_line_series(), es wird allerdings nur eine Liste von  */
/* y-Koordinaten uebergeben. Die x-Koordinaten sind immer: 0, 1, 2 usw...   */
/* ------------------------------------------------------------------------ */
void chart_line_series1(size_t n, const double *y, int color)
{
    size_t i;
    assert(y != NULL);
    for(i = 0; i < n; ++i)
    {
        if(!i) { chart_moveto(0.0, y[i]); continue; }
        assert(chart_state.num_objects < CHART_MAX_OBJECTS);
        if(chart_state.num_objects >= CHART_MAX_OBJECTS) return;
        chart_lineto((double)i, y[i], color);
    }
}

/* ------------------------------------------------------------------------ */
/* Markierung am angegebenen Punkt ausgeben. Fuer den Parameter "color"     */
/* sind folgende Werte moeglich: CHART_BLACK, CHART_RED, CHART_GREEN,       */
/* CHART_BLUE und CHART_GRAY.                                               */
/* ------------------------------------------------------------------------ */
void chart_point(double x, double y, int color)
{
    assert(chart_state.num_objects < CHART_MAX_OBJECTS);
    if(chart_state.num_objects < CHART_MAX_OBJECTS)
    {
        OBJECT *obj = &chart_objects[chart_state.num_objects++];
        obj->x1 = obj->x2 = x;
        obj->y1 = obj->y2 = y;
        obj->color = color;
        obj->type = POINT_OBJECT;
    }
}

/* ------------------------------------------------------------------------ */
/* Eine Punktesequenz aus "n" aufeinanderfolgenden x- bzw. y-Koordinaten    */
/* wird in einem Zug dargestellt. Die aktuelle Ausgabeposition wird durch   */
/* den Aufruf dieser Funktion nicht veraendert. Fuer den Parameter "col"    */
/* sind die folgenden Werte moeglich: CHART_BLACK, CHART_RED, CHART_GREEN,  */
/* CHART_BLUE, CHART_GRAY. (Der Aufrufer muss sicherstellen, dass "x" und   */
/* "y" jeweils auf den Beginn eines Vektors mit "n" double-Werten zeigen!)  */
/* ------------------------------------------------------------------------ */
void chart_point_series(size_t n, const double *x, const double *y, int col)
{
    size_t i;
    assert(x != NULL && y != NULL);
    for(i = 0; i < n; ++i)
    {
        assert(chart_state.num_objects < CHART_MAX_OBJECTS);
        if(chart_state.num_objects >= CHART_MAX_OBJECTS) return;
        chart_point(x[i], y[i], col);
    }
}

/* ------------------------------------------------------------------------ */
/* Variante von chart_point_series(), es wird allerdings nur eine Liste von */
/* y-Koordinaten uebergeben. Die x-Koordinaten sind immer: 0, 1, 2 usw...   */
/* ------------------------------------------------------------------------ */
void chart_point_series1(size_t n, const double *y, int color)
{
    size_t i;
    assert(y != NULL);
    for(i = 0; i < n; ++i)
    {
        assert(chart_state.num_objects < CHART_MAX_OBJECTS);
        if(chart_state.num_objects >= CHART_MAX_OBJECTS) return;
        chart_point((double)i, y[i], color);
    }
}

/* ------------------------------------------------------------------------ */
/* Es wird ein animiertes Chart aufgebaut, das aus mehreren Einzelbildern   */
/* (sog. Frames) besteht. Der Aufruf von chart_end_of_frame() bewirkt,      */
/* dass ein weiteres Einzelbild zum Chart hinzugefuegt wird - nachfolgende  */
/* Aufrufe von chart_point(), chart_moveto() bzw. chart_lineto() beziehen   */
/* sich auf das neue Einzelbild. Die aktuelle Ausgabeposition wird durch    */
/* chart_end_of_frame() auf (0; 0) zurueckgesetzt.                          */
/*                                                                          */
/* Beispiel: Soll eine Animation erstellt werden, die aus lediglich zwei    */
/* Einzelbildern besteht, so werden zunaechst die Punkte und Linien des     */
/* ersten Einzelbildes mittels chart_point() usw. definiert. Es folgt der   */
/* Aufruf von chart_end_of_frame(). Nun werden die Punkte und Linien des    */
/* zweiten Einzelbildes definiert und schliesslich chart_show() zur Anzeige */
/* auf dem Bildschirm aufgerufen. Beide Einzelbilder werden automatisch     */
/* abwechselnd angezeigt, die Geschwindigkeit der Animation kann mittels    */
/* chart_frame_timer() eingestellt werden.                                  */
/* ------------------------------------------------------------------------ */
void chart_end_of_frame(void)
{
    assert(chart_state.num_objects < CHART_MAX_OBJECTS);
    if(chart_state.num_objects < CHART_MAX_OBJECTS)
    {
        OBJECT *obj = &chart_objects[chart_state.num_objects++];
        obj->type = END_OF_FRAME;
        obj->x1 = chart_state.current_x;
        obj->y1 = chart_state.current_y;
        chart_moveto(0, 0);
    }
}

/* ------------------------------------------------------------------------ */
/* Bei einem animierten Chart kann durch chart_frame_timer() eingestellt    */
/* werden, wie lange jedes Einzelbild (sog. Frame) auf dem Bildschirm       */
/* angezeigt wird, siehe auch: chart_end_of_frame(). Minimal koennen        */
/* 100 msec, maximal 10000 msec eingestellt werden.                         */
/* ------------------------------------------------------------------------ */
void chart_frame_timer(int msec)
{
    assert(msec >= 100 && msec <= 10000);
    if(msec >= 100 && msec <= 10000) chart_state.frame_timer_msec = msec;
}

/* ------------------------------------------------------------------------ */
/* Alle Linien und Punkte loeschen, Ausgabeposition auf (0; 0) setzen.      */
/* ------------------------------------------------------------------------ */
void chart_clear(void)
{
    chart_state.num_objects = chart_state.current_frame_start = 0;
    chart_state.zoom_factor = chart_state.zoom_x = chart_state.zoom_y = 0;
    chart_state.background_color = CHART_COLOR;

    chart_moveto(0, 0);
    chart_axis_fmt(AXIS_DEFLT_FMT, AXIS_DEFLT_FMT);
    chart_frame_timer(1000);
    chart_axis_xlimit(-AXIS_DEFLT_LIM, +AXIS_DEFLT_LIM);
    chart_axis_ylimit(-AXIS_DEFLT_LIM, +AXIS_DEFLT_LIM);
}

/* ------------------------------------------------------------------------ */
/* Anzahl der Objekte (Punkte, Linienstuecke usw.) im Chart abfragen.       */
/* ------------------------------------------------------------------------ */
size_t chart_size(void)
{
    return chart_state.num_objects;
}

/* ------------------------------------------------------------------------ */
/* Formatstrings zur Beschriftung der x- und y-Achsen einstellen,           */
/* Default = "%g". Die Formatstrings sehen aehnlich aus wie bei der         */
/* Bibliotheksfunktion "printf" (fuer Beispiele vergl. HMChart.pdf).        */
/* ------------------------------------------------------------------------ */
void chart_axis_fmt(const char *xfmt, const char *yfmt)
{
    if(!xfmt || !yfmt)
    {
        assert(!"null parameter");
        return;
    }

    if(strlen(xfmt) >= AXIS_FMT_LEN || strlen(yfmt) >= AXIS_FMT_LEN)
    {
        assert(!"format string too long");
        return;
    }

    strncpy(chart_state.axis_xfmt, xfmt, AXIS_FMT_LEN);
    strncpy(chart_state.axis_yfmt, yfmt, AXIS_FMT_LEN);
    chart_state.axis_xfmt[AXIS_FMT_LEN - 1] = 0;
    chart_state.axis_yfmt[AXIS_FMT_LEN - 1] = 0;
}

/* ------------------------------------------------------------------------ */
/* Manuelle Achsenskalierung: Minimal-/Maximalwerte fuer x-Achse angeben.   */
/* Um die manuelle Achsenskalierung fuer die x-Achse zu aktivieren, muss    */
/* beim Aufruf von "chart_show" die Option "CHART_XLIMIT" angegeben sein.   */
/* ------------------------------------------------------------------------ */
void chart_axis_xlimit(double xmin, double xmax)
{
    assert(xmax > xmin);
    if(xmax > xmin)
        chart_state.axis_xmin = xmin,
        chart_state.axis_xmax = xmax;
}

/* ------------------------------------------------------------------------ */
/* Manuelle Achsenskalierung: Minimal-/Maximalwerte fuer y-Achse angeben.   */
/* Um die manuelle Achsenskalierung fuer die y-Achse zu aktivieren, muss    */
/* beim Aufruf von "chart_show" die Option "CHART_YLIMIT" angegeben sein.   */
/* ------------------------------------------------------------------------ */
void chart_axis_ylimit(double ymin, double ymax)
{
    assert(ymax > ymin);
    if(ymax > ymin)
        chart_state.axis_ymin = ymin,
        chart_state.axis_ymax = ymax;
}

/* ------------------------------------------------------------------------ */
/* Unterschreitet die Breite bzw. Hoehe des Charts eine bestimmte Grenze,   */
/* wird die Zahl der vertikalen bzw. horizontalen Gitterlinien verringert.  */
/* Mittels "chart_grid_threshold" koennen diese Grenzen eingestellt werden. */
/* ------------------------------------------------------------------------ */
void chart_grid_threshold(int width, int height)
{
    assert(width > 0); assert(height > 0);
    chart_state.grid_width_threshold = width;
    chart_state.grid_heigth_threshold = height;
}

/* ------------------------------------------------------------------------ */
/* Hintergrundfarbe des Charts einstellen (Standardfarbe = Hellgrau).       */
/* In den Parametern r, g und b werden die Rot-, Gruen- und Blau-Anteile    */
/* der gewuenschten Farbe uebergeben (jeweils im Bereich 0...255).          */
/* ------------------------------------------------------------------------ */
void chart_background(unsigned char r, unsigned char g, unsigned char b)
{
    chart_state.background_color = RGB(r, g, b);
}

/* ------------------------------------------------------------------------ */
/* Chart anzeigen und warten, bis der Anwender das Fenster schliesst.       */
/*                                                                          */
/* Fuer den Parameter "options" sind die Werte CHART_DEFAULT,               */
/* CHART_XLIMIT, CHART_YLIMIT, CHART_NOBACKGROUND, CHART_NOGRID,            */
/* CHART_XZOOM_OFF, CHART_YZOOM_OFF und CHART_NOCLEAR erlaubt.              */
/* Mehrere Optionen koennen mit dem |-Operator kombiniert werden:           */
/*                                                                          */
/* - So wird bspw. durch (CHART_NOGRID | CHART_NOBACKGROUND) ein            */
/*   Chart ohne Hintergrund und Koordinatensystem angezeigt.                */
/* - Mittels CHART_XLIMIT und CHART_YLIMIT kann die automatische            */
/*   Achsenskalierung deaktiviert werden; der darstellbare Bereich wird     */
/*   dann ueber chart_axis_xlimit() und chart_axis_ylimit() eingestellt.    */
/* - Mittels CHART_XZOOM_OFF und CHART_YZOOM_OFF kann die                   */
/*   Zoom-Funktionalitaet des Charts in X- oder Y-Richtung oder             */
/*   auch vollstaendig deaktiviert werden. Zum vollstaendigen Deaktivieren  */
/*   ist (CHART_XZOOM_OFF | CHART_YZOOM_OFF) an chart_show() zu uebergeben. */
/* - Wenn chart_show() mit der Option CHART_NOCLEAR aufgerufen wird,        */
/*   werden die Chart-Objekte (Linien, Punkte usw.) nach dem Schliessen     */
/*   des Charts nicht geloescht. Zum "manuellen Loeschen" muss dann         */
/*   ggf. die Funktion chart_clear() aufgerufen werden.                     */
/* ------------------------------------------------------------------------ */
void chart_show(int options, const char *title)
{
    struct timeval prev_time, current_time;
    unsigned long black, white;
    int s, ok;
    XSizeHints hints;
    Atom wmDelete;
    Display *d;
    Visual *v;
    Window w;
    XEvent e;

    /* Verbindung zum X Server herstellen */
    d = XOpenDisplay(NULL);
    if(!d) { fprintf(stderr, "HMChart: Failed to open display.\n"); exit(1); }
 
    s = DefaultScreen(d), v = DefaultVisual(d, s);
    if(v->VISUAL_CLASS != TrueColor)
    { fprintf(stderr, "HMChart: True color display required.\n"); exit(1); }
        
    /* Fenster erzeugen */
    black = BlackPixel(d, s), white = WhitePixel(d, s);
    w = XCreateSimpleWindow(d, RootWindow(d, s), 0, 0,
        DEFAULT_WIDTH, DEFAULT_HEIGHT, 1, black, white);
    assert(w);
    
    /* Fenstertitel ausgeben */
    chart_state.options = options;
    ok = XStoreName(d, w, title ? title : DEFAULT_TITLE);
    assert(ok);

    /* Minimale Fensterabmessungen setzen */
    hints.flags      = PMinSize;
    hints.min_height = MIN_HEIGHT;
    hints.min_width  = MIN_WIDTH;
    XSetWMNormalHints(d, w, &hints);

    /* Welche Ereignisse sind fuer uns interessant? */
    ok = XSelectInput(d, w, ExposureMask | ButtonPressMask | KeyPressMask);
    assert(ok);

    wmDelete = XInternAtom(d, "WM_DELETE_WINDOW", True);
    assert(wmDelete);
    
    ok = XSetWMProtocols(d, w, &wmDelete, 1);
    assert(ok);
 
    /* Fenster auf Bildschirm anzeigen */
    ok = XMapWindow(d, w);
    assert(ok);

    /* Hauptschleife zur Verarbeitung der Fenster-Ereignisse */
    gettimeofday(&prev_time, 0);
    for(;;)
    {
        int delta, update_required = 0;
        while(XPending(d))
        {
            XNextEvent(d, &e);  /* Naechstes Fenster-Ereignis? */
            switch(e.type)
            {
            case Expose:  /* Fenster neu zeichnen */
                 update_required = 1;
                 break;
            case ButtonPress:  /* Zoom in/out */
                 if(e.xbutton.button == 1) zoom_in(d, w);
                 if(e.xbutton.button == 3) zoom_out();
                 paint(d, w);
                 break;
            case KeyPress:  /* 'G' = Gitter ein/aus, 'Q' = Schliessen */
                 if(e.xkey.keycode == XKeysymToKeycode(d, XK_G))
                 { grid_on_off(); paint(d, w); }
                 if(e.xkey.keycode == XKeysymToKeycode(d, XK_Q))
                 { goto chart_show_exit; }
                 break;
            case ClientMessage:  /* Chart beenden */
                 goto chart_show_exit;
            }
        }

        /* Falls erforderlich, wird jetzt das Chart neu gezeichnet. */
        if(update_required) paint(d, w);

        /* Bei animierten Charts wird regelmaessig ueberprueft,
           ob die Zeit fuer ein neues Einzelbild gekommen ist. */
        gettimeofday(&current_time, 0);
        delta = diff_msec(&prev_time, &current_time);
        if(delta > chart_state.frame_timer_msec || delta < 0)
        {
            prev_time = current_time;
            next_frame(d, w);
        }
        else
        {
            /* Nichts zu tun --> 25 msec abwarten! */
            struct timespec ts = {0, 25000000};
            nanosleep(&ts, NULL);
        }
    }
    
chart_show_exit:
    /* 2020-07-27: Chart-Inhalt wird beim Beenden geloescht. */
    if((~options) & CHART_NOCLEAR) chart_clear();
    XCloseDisplay(d);
}
