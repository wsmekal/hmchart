/* ------------------------------------------------------------------------ */
/* chart.h - Ausgabe von 2D-Charts unter Microsoft Windows                  */
/*                                                                          */
/*                  Copyright Tilman Kuepper 2020.                          */
/*    Distributed under the Boost Software License, Version 1.0.            */
/*        (See accompanying file LICENSE_1_0.txt or copy at                 */
/*               http://www.boost.org/LICENSE_1_0.txt)                      */
/*                                                                          */
/* Letzte Bearbeitung: 27. Juli 2020                                        */
/* ------------------------------------------------------------------------ */

#ifndef CHART_H_Included
#define CHART_H_Included

#include <stddef.h>  /* size_t */

#ifdef __cplusplus
extern "C" {
#endif

/* ------------------------------------------------------------------------ */
/* Verschiedene Darstellungsoptionen zur Uebergabe an chart_show();         */
/* mehrere Optionen koennen mit dem |-Operator kombiniert werden:           */
/*                                                                          */
/* - So wird bspw. durch (CHART_NOGRID | CHART_NOBACKGROUND) ein            */
/*   Chart ohne Hintergrund und Koordinatensystem angezeigt.                */
/* - Mittels CHART_XLIMIT und CHART_YLIMIT kann die automatische            */
/*   Achsenskalierung deaktiviert werden; der darstellbare Bereich wird     */
/*   dann ueber chart_axis_xlimit() und chart_axis_ylimit() eingestellt.    */
/* - Mittels CHART_XZOOM_OFF und CHART_YZOOM_OFF kann die                   */
/*   Zoom-Funktionalitaet des Charts in X- oder Y-Richtung oder             */
/*   auch vollstandig deaktiviert werden. Zum vollstaendigen Deaktivieren   */
/*   ist (CHART_XZOOM_OFF | CHART_YZOOM_OFF) an chart_show() zu uebergeben. */
/* - Wenn chart_show() mit der Option CHART_NOCLEAR aufgerufen wird,        */
/*   werden die Chart-Objekte (Linien, Punkte usw.) nach dem Schliessen     */
/*   des Charts nicht geloescht. Zum "manuellen Loeschen" muss dann         */
/*   ggf. die Funktion chart_clear() aufgerufen werden.                     */
/* ------------------------------------------------------------------------ */
#define CHART_DEFAULT       0
#define CHART_NOGRID        1
#define CHART_NOBACKGROUND  2
#define CHART_XLIMIT        4
#define CHART_YLIMIT        8
#define CHART_XZOOM_OFF    16
#define CHART_YZOOM_OFF    32
#define CHART_NOCLEAR      64

/* ------------------------------------------------------------------------ */
/* Moegliche Farben fuer chart_lineto() und chart_point()                   */
/* ------------------------------------------------------------------------ */
#define CHART_BLACK 0
#define CHART_RED   1
#define CHART_GREEN 2
#define CHART_BLUE  3
#define CHART_GRAY  4

/* ------------------------------------------------------------------------ */
/* Aktuelle Ausgabeposition setzen.                                         */
/* ------------------------------------------------------------------------ */
void chart_moveto(double x, double y);

/* ------------------------------------------------------------------------ */
/* Linie von der aktuellen Ausgabeposition zum angegebenen Punkt zeichnen.  */
/* Fuer den Parameter "color" sind folgende Werte moeglich: CHART_BLACK,    */
/* CHART_RED, CHART_GREEN, CHART_BLUE und CHART_GRAY.                       */
/* ------------------------------------------------------------------------ */
void chart_lineto(double x, double y, int color);

/* ------------------------------------------------------------------------ */
/* Eine komplette Kurve aus "n" aufeinanderfolgenden x- bzw. y-Koordinaten  */
/* wird in einem Zug dargestellt. Die aktuelle Ausgabeposition befindet     */
/* sich anschliessend am letzten Stuetzpunkt der Kurve. Fuer den Parameter  */
/* "color" sind die folgenden Werte moeglich: CHART_BLACK, CHART_RED,       */
/* CHART_GREEN, CHART_BLUE und CHART_GRAY. (Der Aufrufer dieser Funktion    */
/* muss sicherstellen, dass die Parameter "x" und "y" jeweils auf den       */
/* Beginn eines Vektors mit "n" double-Werten zeigen!)                      */
/* ------------------------------------------------------------------------ */
void chart_line_series(size_t n, const double *x, const double *y, int color);

/* ------------------------------------------------------------------------ */
/* Variante von chart_line_series(), es wird allerdings nur eine Liste von  */
/* y-Koordinaten uebergeben. Die x-Koordinaten sind immer: 0, 1, 2 usw...   */
/* ------------------------------------------------------------------------ */
void chart_line_series1(size_t n, const double *y, int color);

/* ------------------------------------------------------------------------ */
/* Markierung am angegebenen Punkt ausgeben. Fuer den Parameter "color"     */
/* sind folgende Werte moeglich: CHART_BLACK, CHART_RED, CHART_GREEN,       */
/* CHART_BLUE und CHART_GRAY.                                               */
/* ------------------------------------------------------------------------ */
void chart_point(double x, double y, int color);

/* ------------------------------------------------------------------------ */
/* Eine Punktesequenz aus "n" aufeinanderfolgenden x- bzw. y-Koordinaten    */
/* wird in einem Zug dargestellt. Die aktuelle Ausgabeposition wird durch   */
/* den Aufruf dieser Funktion nicht veraendert. Fuer den Parameter "col"    */
/* sind die folgenden Werte moeglich: CHART_BLACK, CHART_RED, CHART_GREEN,  */
/* CHART_BLUE, CHART_GRAY. (Der Aufrufer muss sicherstellen, dass "x" und   */
/* "y" jeweils auf den Beginn eines Vektors mit "n" double-Werten zeigen!)  */
/* ------------------------------------------------------------------------ */
void chart_point_series(size_t n, const double *x, const double *y, int col);

/* ------------------------------------------------------------------------ */
/* Variante von chart_point_series(), es wird allerdings nur eine Liste von */
/* y-Koordinaten uebergeben. Die x-Koordinaten sind immer: 0, 1, 2 usw...   */
/* ------------------------------------------------------------------------ */
void chart_point_series1(size_t n, const double *y, int color);

/* ------------------------------------------------------------------------ */
/* Es wird ein animiertes Chart aufgebaut, das aus mehreren Einzelbildern   */
/* (sog. Frames) besteht. Der Aufruf von chart_end_of_frame() bewirkt,      */
/* dass ein weiteres Einzelbild zum Chart hinzugefuegt wird - nachfolgende  */
/* Aufrufe von chart_point(), chart_moveto() bzw. chart_lineto() beziehen   */
/* sich auf das neue Einzelbild. Die aktuelle Ausgabeposition wird durch    */
/* chart_end_of_frame() auf (0; 0) zurueckgesetzt.                          */
/*                                                                          */
/* Beispiel: Soll eine Animation erstellt werden, die aus lediglich zwei    */
/* Einzelbildern besteht, so werden zunaechst die Punkte und Linien des     */
/* ersten Einzelbildes mittels chart_point() usw. definiert. Es folgt der   */
/* Aufruf von chart_end_of_frame(). Nun werden die Punkte und Linien des    */
/* zweiten Einzelbildes definiert und schliesslich chart_show() zur Anzeige */
/* auf dem Bildschirm aufgerufen. Beide Einzelbilder werden automatisch     */
/* abwechselnd angezeigt, die Geschwindigkeit der Animation kann mittels    */
/* chart_frame_timer() eingestellt werden.                                  */
/* ------------------------------------------------------------------------ */
void chart_end_of_frame(void);

/* ------------------------------------------------------------------------ */
/* Bei einem animierten Chart kann durch chart_frame_timer() eingestellt    */
/* werden, wie lange jedes Einzelbild (sog. Frame) auf dem Bildschirm       */
/* angezeigt wird, siehe auch: chart_end_of_frame(). Minimal koennen        */
/* 100 msec, maximal 10000 msec eingestellt werden.                         */
/* ------------------------------------------------------------------------ */
void chart_frame_timer(int msec);

/* ------------------------------------------------------------------------ */
/* Alle Linien und Punkte loeschen, Ausgabeposition auf (0; 0) setzen.      */
/* ------------------------------------------------------------------------ */
void chart_clear(void);

/* ------------------------------------------------------------------------ */
/* Anzahl der Objekte (Punkte, Linienstuecke usw.) im Chart abfragen.       */
/* ------------------------------------------------------------------------ */
size_t chart_size(void);

/* ------------------------------------------------------------------------ */
/* Formatstrings zur Beschriftung der x- und y-Achsen einstellen,           */
/* Default = "%g". Die Formatstrings sehen aehnlich aus wie bei der         */
/* Bibliotheksfunktion "printf" (fuer Beispiele vergl. HMChart.pdf).        */
/* ------------------------------------------------------------------------ */
void chart_axis_fmt(const char *xfmt, const char *yfmt);

/* ------------------------------------------------------------------------ */
/* Manuelle Achsenskalierung: Minimal-/Maximalwerte fuer x-Achse angeben.   */
/* Um die manuelle Achsenskalierung fuer die x-Achse zu aktivieren, muss    */
/* beim Aufruf von "chart_show" die Option "CHART_XLIMIT" angegeben sein.   */
/* ------------------------------------------------------------------------ */
void chart_axis_xlimit(double xmin, double xmax);

/* ------------------------------------------------------------------------ */
/* Manuelle Achsenskalierung: Minimal-/Maximalwerte fuer y-Achse angeben.   */
/* Um die manuelle Achsenskalierung fuer die y-Achse zu aktivieren, muss    */
/* beim Aufruf von "chart_show" die Option "CHART_YLIMIT" angegeben sein.   */
/* ------------------------------------------------------------------------ */
void chart_axis_ylimit(double ymin, double ymax);

/* ------------------------------------------------------------------------ */
/* Unterschreitet die Breite bzw. Hoehe des Charts eine bestimmte Grenze,   */
/* wird die Zahl der vertikalen bzw. horizontalen Gitterlinien verringert.  */
/* Mittels "chart_grid_threshold" koennen diese Grenzen eingestellt werden. */
/* ------------------------------------------------------------------------ */
void chart_grid_threshold(int width, int height);

/* ------------------------------------------------------------------------ */
/* Hintergrundfarbe des Charts einstellen (Standardfarbe = Hellgrau).       */
/* In den Parametern r, g und b werden die Rot-, Gruen- und Blau-Anteile    */
/* der gewuenschten Farbe uebergeben (jeweils im Bereich 0...255).          */
/* ------------------------------------------------------------------------ */
void chart_background(unsigned char r, unsigned char g, unsigned char b);

/* ------------------------------------------------------------------------ */
/* Chart anzeigen und warten, bis der Anwender das Fenster schliesst.       */
/*                                                                          */
/* Fuer den Parameter "options" sind die Werte CHART_DEFAULT,               */
/* CHART_XLIMIT, CHART_YLIMIT, CHART_NOBACKGROUND, CHART_NOGRID,            */
/* CHART_XZOOM_OFF, CHART_YZOOM_OFF und CHART_NOCLEAR erlaubt.              */
/* Mehrere Optionen koennen mit dem |-Operator kombiniert werden:           */
/*                                                                          */
/* - So wird bspw. durch (CHART_NOGRID | CHART_NOBACKGROUND) ein            */
/*   Chart ohne Hintergrund und Koordinatensystem angezeigt.                */
/* - Mittels CHART_XLIMIT und CHART_YLIMIT kann die automatische            */
/*   Achsenskalierung deaktiviert werden; der darstellbare Bereich wird     */
/*   dann ueber chart_axis_xlimit() und chart_axis_ylimit() eingestellt.    */
/* - Mittels CHART_XZOOM_OFF und CHART_YZOOM_OFF kann die                   */
/*   Zoom-Funktionalitaet des Charts in X- oder Y-Richtung oder             */
/*   auch vollstaendig deaktiviert werden. Zum vollstaendigen Deaktivieren  */
/*   ist (CHART_XZOOM_OFF | CHART_YZOOM_OFF) an chart_show() zu uebergeben. */
/* - Wenn chart_show() mit der Option CHART_NOCLEAR aufgerufen wird,        */
/*   werden die Chart-Objekte (Linien, Punkte usw.) nach dem Schliessen     */
/*   des Charts nicht geloescht. Zum "manuellen Loeschen" muss dann         */
/*   ggf. die Funktion chart_clear() aufgerufen werden.                     */
/* ------------------------------------------------------------------------ */
void chart_show(int options, const char *title);

#ifdef __cplusplus
}
#endif

/* ------------------------------------------------------------------------ */
/* Im "C++-Modus" gibt es zusaetzliche Versionen von chart_line_series()    */
/* und chart_point_series(). Dadurch ist es einfacher moeglich, komplette   */
/* Kurven zu zeichnen, wenn die x- bzw. y-Koordinaten in Containerklassen   */
/* gespeichert sind (zum Beispiel in einem std::vector<double>).            */
/* ------------------------------------------------------------------------ */
#if defined(__cplusplus) && !defined(CHART_NO_CPP)

#include <assert.h>
#include <vector>

/* ------------------------------------------------------------------------ */
/* Eine Punktesequenz aus aufeinanderfolgenden x- bzw. y-Koordinaten        */
/* wird in einem Zug dargestellt. Die x- bzw. y-Koordinaten sind in         */
/* Containerklassen gespeichert. Die aktuelle Ausgabeposition wird durch    */
/* den Aufruf dieser Funktion nicht veraendert. Fuer den Parameter "col"    */
/* sind die folgenden Werte moeglich: CHART_BLACK, CHART_RED, CHART_GREEN,  */
/* CHART_BLUE, CHART_GRAY.                                                  */
/* ------------------------------------------------------------------------ */
template <class Vector>
void chart_point_series(const Vector& xvec, const Vector& yvec, int color)
{
    std::vector<double> x(xvec.begin(), xvec.end());
    std::vector<double> y(yvec.begin(), yvec.end());
    if(x.size() == y.size())
        chart_point_series(x.size(), x.data(), y.data(), color);
    else
        assert(!"chart_point_series: xvec, yvec size mismatch");
}

/* ------------------------------------------------------------------------ */
/* Siehe Dokumentation zu chart_point_series1()...                          */
/* ------------------------------------------------------------------------ */
template <class Vector>
void chart_point_series(const Vector& yvec, int color)
{
    std::vector<double> y(yvec.begin(), yvec.end());
    if(y.size() > 0) chart_point_series1(y.size(), y.data(), color);
}

/* ------------------------------------------------------------------------ */
/* Eine komplette Kurve aus aufeinanderfolgenden x- bzw. y-Koordinaten      */
/* wird in einem Zug dargestellt. Die x- bzw. y-Koordinaten sind in         */
/* Containerklassen gespeichert. Die aktuelle Ausgabeposition wird durch    */
/* den Aufruf dieser Funktion nicht veraendert. Fuer den Parameter "col"    */
/* sind die folgenden Werte moeglich: CHART_BLACK, CHART_RED, CHART_GREEN,  */
/* CHART_BLUE, CHART_GRAY.                                                  */
/* ------------------------------------------------------------------------ */
template <class Vector>
void chart_line_series(const Vector& xvec, const Vector& yvec, int color)
{
    std::vector<double> x(xvec.begin(), xvec.end());
    std::vector<double> y(yvec.begin(), yvec.end());
    if(x.size() == y.size() && x.size() > 1)
        chart_line_series(x.size(), x.data(), y.data(), color);
    else
        assert(!"chart_line_series: xvec, yvec size mismatch");
}

/* ------------------------------------------------------------------------ */
/* Siehe Dokumentation zu chart_line_series1()...                           */
/* ------------------------------------------------------------------------ */
template <class Vector>
void chart_line_series(const Vector& yvec, int color)
{
    std::vector<double> y(yvec.begin(), yvec.end());
    if(y.size() > 1) chart_line_series1(y.size(), y.data(), color);
}

#endif  /* __cplusplus      */
#endif  /* CHART_H_Included */
